<?php




Route::get('/', 'Front\RestaurantFrontController@frontPage');


Auth::routes();

Route::get('/profile', 'HomeController@userProfile')->name('user.profile');
Route::post('/profile/{name}', 'HomeController@updateUserProfile');

Route::get('/orders', 'HomeController@userOrder')->name('user.orders');
Route::get('/change-email-password', 'HomeController@showEmailPassword')->name('user.emailPassword');
Route::post('/change-email', 'HomeController@updateUserProfileEmail')->name('change.user.email');
Route::post('/change-password', 'HomeController@updateUserProfilePassword')->name('change.user.password');



// For Admin
Route::get('admin/home', 'AdminController@index')->name('admin.dashboard');
Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');

Route::post('admin/logout', 'Admin\LoginController@adminLogout')->name('admin.logout');

Route::post('admin', 'Admin\LoginController@login');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset');
Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::get('admin/register', 'Admin\RegisterController@showRegistrationForm')->name('admin.register');
Route::post('admin/register', 'Admin\RegisterController@register');


// For FAQ
Route::get('/faq-title/add-faq-title', 'ManageAdmin\FaqController@addFaqTitleInfo')->name('add.faq.title');
Route::post('/faq-title/new-faq-title', 'ManageAdmin\FaqController@saveFaqTitleInfo');
Route::get('/faq-title/manage-faq-title', 'ManageAdmin\FaqController@manageFaqTitleInfo')->name('manage.faq.title');
Route::get('/faq-title/unpublished-faq-title/{id}', 'ManageAdmin\FaqController@unpublishedFaqTitleInfo');
Route::get('/faq-title/published-faq-title/{id}', 'ManageAdmin\FaqController@publishedFaqTitleInfo');
Route::get('/faq-title/edit-faq-title/{id}', 'ManageAdmin\FaqController@editFaqTitleInfo');
Route::post('/faq-title/update-faq-title/{id}', 'ManageAdmin\FaqController@updateFaqTitleInfo');
Route::get('/faq-title/delete-faq-title/{id}', 'ManageAdmin\FaqController@deleteFaqTitleInfo');


// For FAQ Content
Route::get('/faq-content/add-faq-content', 'ManageAdmin\FaqContentController@addFaqContentInfo')->name('add.faq.content');
Route::post('/faq-content/new-faq-question-content', 'ManageAdmin\FaqContentController@saveFaqQuestionContentInfo');
Route::post('/faq-content/new-faq-video-content', 'ManageAdmin\FaqContentController@saveFaqVideoContentInfo');
Route::get('/faq-content/manage-faq-content', 'ManageAdmin\FaqContentController@manageFaqContentInfo')->name('manage.faq.content');
Route::get('/faq-content/unpublished-faq-content/{id}', 'ManageAdmin\FaqContentController@unpublishedFaqContentInfo');
Route::get('/faq-content/published-faq-content/{id}', 'ManageAdmin\FaqContentController@publishedFaqContentInfo');
Route::get('/faq-content/edit-faq-content/{id}', 'ManageAdmin\FaqContentController@editFaqContentInfo');
Route::post('/faq-content/update-faq-content/{id}', 'ManageAdmin\FaqContentController@updateFaqContentInfo');
Route::get('/faq-content/delete-faq-content/{id}', 'ManageAdmin\FaqContentController@deleteFaqContentInfo');


//Manage User
Route::get('/manage/user', 'ManageAdmin\UserController@manageUser')->name('manage.user');
Route::get('/user/{id}/delete-user', 'ManageAdmin\UserController@deleteUser');

Route::get('/manage/city', 'ManageAdmin\CityController@manageCity')->name('manage.city');
Route::get('/add/city', 'ManageAdmin\CityController@viewCityForm')->name('add.city');
Route::post('/add/new-city', 'ManageAdmin\CityController@addCity');
Route::get('/city/edit-city/{id}', 'ManageAdmin\CityController@editCity');
Route::post('/city/update-city/{id}', 'ManageAdmin\CityController@updateCity');
Route::get('/city/delete-city/{id}', 'ManageAdmin\CityController@deleteCity');


Route::get('/manage/sub-header-image', 'ManageAdmin\SubheaderImageController@showSubHeaderImages')->name('manage.subHeaderImage');
Route::get('/add/sub-header-image', 'ManageAdmin\SubheaderImageController@showSubHeaderImagesForm')->name('add.subHeaderImage');
Route::post('add/new-sub-header-image', 'ManageAdmin\SubheaderImageController@saveSubHeaderImages');
Route::get('delete-sub-header-image/{id}', 'ManageAdmin\SubheaderImageController@deleteSubHeaderImage');



// Manage Restaurant Owner

Route::get('manage/restaurant-owner-info', 'ManageAdmin\RestaurantOwnerController@manageRestaurantOwner')->name('manage.restaurant.owner');
Route::get('/restaurant-owner/{id}/restaurant-owner-info', 'ManageAdmin\RestaurantOwnerController@viewRestaurantOwnerInfo');
Route::get('/restaurant-owner/{id}/verify-restaurant-owner', 'ManageAdmin\RestaurantOwnerController@verifyRestaurantOwner');
Route::get('/restaurant-owner/{id}/notverified-restaurant-owner', 'ManageAdmin\RestaurantOwnerController@notverifyRestaurantOwner');

Route::get('/restaurant-owner/{id}/add-popularity-restaurant-owner', 'ManageAdmin\RestaurantOwnerController@verifyPopularityRestaurantOwner');
Route::get('/restaurant-owner/{id}/cancel-popularity-restaurant-owner', 'ManageAdmin\RestaurantOwnerController@cancelPopularityRestaurantOwner');



Route::get('/restaurant-owner/{id}/delete-restaurant-owner', 'ManageAdmin\RestaurantOwnerController@deleteRestaurantOwnerInfo');



// For rider

Route::get('/rider/manage-rider', 'ManageAdmin\RiderController@manageRiderInfo')->name('manage.rider.admin');
Route::get('/rider/not-hire-rider/{id}', 'ManageAdmin\RiderController@notHireRider');
Route::get('/rider/hire-rider/{id}', 'ManageAdmin\RiderController@hireRider');
Route::get('/rider/delete-rider/{id}', 'ManageAdmin\RiderController@deleteRiderInfo');






//

// For Restaurant Auth
Route::get('restaurant/home', 'RestaurantController@index')->name('restaurant.dashboard');
Route::get('restaurant/login', 'Restaurant\LoginController@showLoginForm')->name('restaurant.login');
Route::post('restaurant/login', 'Restaurant\LoginController@login');
Route::post('restaurant-password/email', 'Restaurant\ForgotPasswordController@sendResetLinkEmail')->name('restaurant.password.email');
Route::get('restaurant-password/reset', 'Restaurant\ForgotPasswordController@showLinkRequestForm')->name('restaurant.password.request');
Route::post('restaurant-password/reset', 'Restaurant\ResetPasswordController@reset');
Route::get('restaurant-password/reset/{token}', 'Restaurant\ResetPasswordController@showResetForm')->name('restaurant.password.reset');

Route::get('restaurant/logout', 'Restaurant\LoginController@logout')->name('restaurant.logout');

Route::get('restaurant/register', 'Restaurant\RegisterController@showRegistrationForm')->name('restaurant.register');
Route::post('restaurant/register', 'Restaurant\RegisterController@register');

//


// Restaurant Auth Edit
Route::get('restaurant-admin/profile', 'ManageRestaurant\RestaurantAdminController@showRestaurantAdminProfile')->name('restaurant-admin.profile');
Route::post('restaurant-admin/{id}', 'ManageRestaurant\RestaurantAdminController@updateRestaurantProfile');
Route::post('restaurant-admin/password/{id}', 'ManageRestaurant\RestaurantAdminController@updateRestaurantProfilePassword');
Route::post('restaurant-admin/email/{id}', 'ManageRestaurant\RestaurantAdminController@updateRestaurantProfileEmail');

// Restaurant Info
Route::get('restaurant-info', 'ManageRestaurant\RestaurantInfoController@showRestaurantInfoForm')->name('show.restaurant-info');
Route::post('restaurant-info/edit', 'ManageRestaurant\RestaurantInfoController@editRestaurantInfo')->name('edit.restaurant-info');

// Restaurant Cuisines
Route::get('restaurant-cuisines', 'ManageRestaurant\RestaurantInfoController@showRestaurantCuisines')->name('restaurant.cuisines');
Route::post('restaurant-cuisines/add', 'ManageRestaurant\RestaurantInfoController@saveRestaurantCuisines')->name('save.restaurant.cuisines');
Route::get('restaurant-cuisines/{id}/edit', 'ManageRestaurant\RestaurantInfoController@editRestaurantCuisines');
Route::post('restaurant-cuisines/update', 'ManageRestaurant\RestaurantInfoController@updateRestaurantCuisines')->name('update.restaurant.cuisines');
Route::get('restaurant-cuisines/{id}', 'ManageRestaurant\RestaurantInfoController@destroyRestaurantCuisines');

// Restaurant Sub-Images
Route::get('restaurant-sub-images', 'ManageRestaurant\RestaurantSubImageController@showRestaurantSubImages')->name('restaurant.sub-images');
Route::post('restaurant-sub-images/add', 'ManageRestaurant\RestaurantSubImageController@saveRestaurantSubImages')->name('save.restaurant.sub-images');
Route::get('restaurant-sub-images/{id}', 'ManageRestaurant\RestaurantSubImageController@destroyRestaurantSubImages');

// Restaurant Open-Close Time
Route::get('restaurant-open-time', 'ManageRestaurant\RestaurantOpenTimeController@showRestaurantOpenTime')->name('restaurant.OpenTime');
Route::post('restaurant-open-time/add', 'ManageRestaurant\RestaurantOpenTimeController@saveRestaurantOpenTime')->name('save.restaurant.OpenTime');


// Restaurant Menu
Route::get('menu/manage-menu', 'ManageRestaurant\RestaurantMenuController@manageRestaurantMenuInfo')->name('restaurant.menu');
Route::post('/menu/new-menu', 'ManageRestaurant\RestaurantMenuController@saveRestaurantMenuInfo')->name('save.restaurant.menu');
Route::get('/menu/unpublished-menu/{id}', 'ManageRestaurant\RestaurantMenuController@unpublishedRestaurantMenuInfo');
Route::get('/menu/published-menu/{id}', 'ManageRestaurant\RestaurantMenuController@publishedRestaurantMenuInfo');
Route::get('/menu/edit-menu/{id}', 'ManageRestaurant\RestaurantMenuController@editRestaurantMenuInfo')->name('edit.restaurant.menu');
Route::post('/menu/update-menu/{id}', 'ManageRestaurant\RestaurantMenuController@updateRestaurantMenuInfo');
Route::get('/menu/delete-menu/{id}', 'ManageRestaurant\RestaurantMenuController@deleteRestaurantMenuInfo');


// Restaurant Menu Item
Route::get('menu-item/manage-menu-item', 'ManageRestaurant\RestaurantMenuItemController@manageRestaurantMenuItemInfo')->name('menu.item');
Route::post('/menu-item/new-menu-item', 'ManageRestaurant\RestaurantMenuItemController@saveRestaurantMenuItemInfo')->name('save.restaurant.menu-item');
Route::get('/menu-item/unpublished-menu-item/{id}', 'ManageRestaurant\RestaurantMenuItemController@unpublishedRestaurantMenuItemInfo');
Route::get('/menu-item/published-menu-item/{id}', 'ManageRestaurant\RestaurantMenuItemController@publishedRestaurantMenuItemInfo');
Route::get('/menu-item/edit-menu-item/{id}', 'ManageRestaurant\RestaurantMenuItemController@editRestaurantMenuItemInfo')->name('edit.restaurant.menu-item');
Route::post('/menu-item/update-menu-item/{id}', 'ManageRestaurant\RestaurantMenuItemController@updateRestaurantMenuItemInfo');
Route::get('/menu-item/delete-menu-item/{id}', 'ManageRestaurant\RestaurantMenuItemController@deleteRestaurantMenuItemInfo');

// Restaurant Menu Item Ingredient
Route::get('/ingredient/manage-ingredient/{id}', 'ManageRestaurant\ItemIngredientController@manageItemIngredientInfo');
Route::post('/ingredient/new-ingredient', 'ManageRestaurant\ItemIngredientController@saveItemIngredientInfo')->name('save.item-ingredient');
Route::get('/ingredient/delete-ingredient/{id}', 'ManageRestaurant\ItemIngredientController@deleteItemIngredientInfo');


// Restaurant Delivery Time
Route::get('/manage/delivery-time', 'ManageRestaurant\RestaurantDeliveryTimeController@manageDeliveryTime')->name('manage.delivery.time');

Route::post('delivery-time/add', 'ManageRestaurant\RestaurantDeliveryTimeController@saveDeliveryTime')->name('save.delivery.time');

//Route::get('delivery-time/{id}/edit', 'ManageRestaurant\RestaurantDeliveryTimeController@editRestaurantCuisines');
//Route::post('delivery-time/update', 'ManageRestaurant\RestaurantDeliveryTimeController@updateRestaurantCuisines')->name('update.restaurant.cuisines');

Route::get('delivery-time/{id}', 'ManageRestaurant\RestaurantDeliveryTimeController@destroyDeliveryTime');







// Front RestaurantFront Controller
Route::get('/restaurant/restaurant-list', 'Front\RestaurantFrontController@showRestaurantGridList')->name('restaurant.list');
Route::get('/restaurant/restaurant-menu/{id}', 'Front\RestaurantFrontController@showRestaurantMenu');
Route::get('/search-city/{cityName}', 'Front\RestaurantFrontController@searchByCity');
Route::get('/search-service/{service}', 'Front\RestaurantFrontController@searchByService');


Route::get('/search-service/rating/{service}', 'Front\RestaurantFrontController@searchByRating');

Route::get('/about-us', 'Front\OtherViewController@viewAboutUs')->name('aboutUs');
Route::get('/faq', 'Front\OtherViewController@viewFAQ')->name('faq');

Route::get('/submit-rider', 'Front\OtherViewController@viewSubmitDriverForm')->name('submit.rider');
Route::post('/new-submit-rider', 'Front\OtherViewController@saveSubmitDriverForm')->name('save.rider');



// Front RestaurantFront Controller
Route::get('/restaurant/restaurant-detail/{id}', 'Front\RestaurantDetailsFrontController@showRestaurantDetail')->name('restaurant.details');
//Route::get('/restaurant/restaurant-detail/{id}', 'Front\RestaurantDetailsFrontController@showRestaurantMenu');


//Cart Start
Route::post('/add-to-cart','Front\CartController@addToCart');
Route::get('/show-cart','Front\CartController@showCart');
Route::post('/update-cart-product','Front\CartController@updateCart');
Route::get('/delete-cart-product/{id}','Front\CartController@deleteCart');
Route::get('/back','Front\CartController@back');
// Cart End



// Fetch Data by Search Form
Route::post('/search', 'Front\RestaurantFrontController@search')->name('search');


// User Sign In, Login
Route::get('/checkout','Front\CheckoutController@viewCheckoutRegisterLoginForm');
Route::post('/new-customer','Front\CheckoutController@saveCustomerInfo');
Route::get('/shipping-info', 'Front\CheckoutController@showShippingInfo');
Route::get('/customer-logout', 'Front\CheckoutController@customerLogout');
Route::post('/customer-login', 'Front\CheckoutController@customerLoginCheck');
Route::post('/new-shipping', 'Front\CheckoutController@saveShippingInfo');
Route::get('/payment-info', 'Front\CheckoutController@showPaymentForm');
Route::post('/new-order', 'Front\CheckoutController@saveOrderInfo');
Route::get('/confirm-order', 'Front\CheckoutController@confirmOrder');



// For order
Route::get('/manage-order', 'ManageRestaurant\OrderController@manageOrderInfo')->name('restaurant.order');
Route::get('/order/view-order-details/{id}', 'ManageRestaurant\OrderController@viewOrderDetail');
Route::get('/order/view-order-invoice/{id}', 'ManageRestaurant\OrderController@viewOrderInvoice');
Route::get('/order/edit-order/{id}', 'ManageRestaurant\OrderController@editOrderInfo');
Route::post('/update-order', 'ManageRestaurant\OrderController@updateOrderInfo');

Route::get('/order/delete-order/{id}', 'ManageRestaurant\OrderController@deleteOrder');
Route::get('/pdf', 'ManageRestaurant\OrderController@invoicePDF'); //use dompdf

// User Review
Route::post('/user/review', 'Front\UserReviewController@userReview');