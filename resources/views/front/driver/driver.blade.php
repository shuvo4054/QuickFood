@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{asset('front-end')}}/css/skins/square/grey.css" rel="stylesheet">

@endsection

@section('subheader')



    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short"  data-parallax="scroll" data-image-src="{{isset($subHeaderImage->sub_header_image)}}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Work With Us</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Rider</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Flexible Job and Great fees</h2>

        </div>
        <div class="row">
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.1s">
                <div class="feature_2">
                    <i class="icon_currency"></i>
                    <h3>Great Fees</h3>
                    <p>
                        Submit your information and earn great fee...
                    </p>
                </div>
            </div>
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.2s">
                <div class="feature_2">
                    <i class="icon_easel"></i>
                    <h3>Growing possibility</h3>
                    <p>
                        Your money growing by your experience...
                    </p>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
                <div class="feature_2">
                    <i class="icon_map_alt"></i>
                    <h3>Work in a small area</h3>
                    <p>
                        Work in your city, town...
                    </p>
                </div>
            </div>
            <div class="col-md-6 wow fadeIn" data-wow-delay="0.5s">
                <div class="feature_2">
                    <i class="icon_clock_alt"></i>
                    <h3>Flexible time</h3>
                    <p>
                        You can work when you are free...
                    </p>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">

            <div class="col-md-6 wow fadeIn" data-wow-delay="0.6s">
                <div class="feature_2">
                    <i class="icon_calendar"></i>
                    <h3>Flexible days</h3>
                    <p>
                        Work in your flexible days...
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 nopadding features-intro-img">
                <div class="features-bg img_2">
                    <div class="features-img">
                    </div>
                </div>
            </div>
            <div class="col-md-6 nopadding">
                <div class="features-content">
                    <h3>"What you'll need"</h3>
                    <ul class="list_ok">
                        <li>A bicycle or scooter/motorbike with relevant safety equipment (road safety is a huge must for us!).</li>
                        <li>Smartphone - iPhone 4s or above or Android 4.3 or above.</li>
                        <li>The right to work in Bangladesh.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- End container-fluid  -->

    <div class="container margin_60">
        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Please submit the form below</h2>

        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                <form  method="POST" action="{{ route('save.rider') }}">
                    {{ csrf_field() }}

                    <div class="row">

                        <div class="col-md-6 col-sm-6">

                                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Mahabub Jahan" value="{{ old('first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">Last name</label>
                                    <input type="text" class="form-control" id="name_contact" name="last_name" placeholder="Shuvo" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                    </div>
                    <div class="row">



                        <div class="col-md-6 col-sm-6">
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control " placeholder="shuvo@email.com" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-6 col-sm-6">

                        <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                            <label for="mobile_number">Mobile number</label>
                            <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="01722222222" value="{{ old('mobile_number') }}" required autofocus>
                        </div>

                        @if ($errors->has('mobile_number'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                        @endif
                        </div>




                    </div>

                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for="city">City</label>
                                <select name="city" class="form-control" id="city">

                                    <option value="" selected="">Select City</option>

                                    @foreach(\App\Http\Controllers\ManageAdmin\CityController::city() as $cities)
                                        <option value="{{ $cities->city_name }}">{{ $cities->city_name }}</option>
                                    @endforeach



                                </select>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="col-md-6 col-sm-6">
                            <div class="form-group {{ $errors->has('living_address') ? ' has-error' : '' }}">
                                <label for="living_address">Living Address</label>
                                <input type="text" id="living_address" name="living_address" class="form-control " placeholder="Living Address" value="{{ old('living_address') }}" required autofocus>
                                @if ($errors->has('living_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('living_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    </div><!-- End row  -->


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('motor') ? ' has-error' : '' }}">
                                <h5>Do you have a motorbike or scooter?</h5>
                                <label><input name="motor" type="radio" value="yes" class="icheck" checked>Yes</label>
                                <label class="margin_left"><input name="motor" type="radio" value="no" class="icheck">No</label>
                                @if ($errors->has('motor'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('motor') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('license') ? ' has-error' : '' }}">
                                <h5>Do you have a driving license?</h5>
                                <label><input name="license" type="radio" value="yes" class="icheck" checked>Yes</label>
                                <label class="margin_left"><input name="license" type="radio" value="no" class="icheck">No</label>
                            </div>
                            @if ($errors->has('license'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('license') }}</strong>
                                    </span>
                            @endif
                        </div>

                    </div><!-- End row  -->
                    <hr style="border-color:#ddd;">
                    <div class="text-center"><button type="submit" class="btn_full_outline">Submit</button></div>
                </form>
            </div><!-- End col  -->
        </div><!-- End row  -->
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection





@section('script')


@endsection
