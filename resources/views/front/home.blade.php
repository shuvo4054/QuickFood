@extends('front.master')


@section('subheader')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="home" data-parallax="scroll" data-image-src="@isset($subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset"
             data-natural-width="1400" data-natural-height="550">
        <div id="subheader">
            <div id="sub_content">
                <h1>Order <strong id="js-rotating">Takeaway,Delivery,Quick</strong> Food</h1>
                <p>

                </p>

                <form method="POST" action="{{ route('search') }}">
                    {{ csrf_field() }}

                    <div id="custom-search-input">
                        <div class="input-group ">
                            <input type="text" name="search_key" class="search-query" placeholder="Search Restaurant by Name, City, Address..">
                            <span class="input-group-btn">
                        <input type="submit" class="btn_search" value="submit">
                        </span>
                        </div>
                    </div>

                </form>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
        <!-- <div id="count" class="hidden-xs">
             <ul>
                 <li><span class="number">2650</span> Restaurant</li>
                 <li><span class="number">5350</span> People Served</li>
                 <li><span class="number">12350</span> Registered Users</li>
             </ul>
         </div>-->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <!-- Content ================================================== -->
    <div class="container margin_60">

        <div class="main_title">
            <h2 class="nomargin_top" style="padding-top:0">How it works</h2>
            {{--<p>
                Cum doctus civibus efficiantur in imperdiet deterruisset.
            </p>--}}
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="box_home" id="one">
                    <span>1</span>
                    <h3>Search by address</h3>
                    <p>
                        Find all restaurants available in your zone.
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_home" id="two">
                    <span>2</span>
                    <h3>Choose a restaurant</h3>
                    <p>
                        We have all type of menus online.
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_home" id="three">
                    <span>3</span>
                    <h3>Pay by cash</h3>
                    <p>
                        It's quick, easy and totally simple.
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_home" id="four">
                    <span>4</span>
                    <h3>Delivery or takeaway</h3>
                    <p>
                        You are lazy? Are you backing home?
                    </p>
                </div>
            </div>
        </div><!-- End row -->

        <div id="delivery_time" class="hidden-xs">
            <strong><span>2</span><span>5</span></strong>
            <h4>The minutes that usually takes to deliver!</h4>
        </div>
    </div><!-- End container -->

    <div class="white_bg">
        <div class="container margin_60">

            <div class="main_title">
                <h2 class="nomargin_top">Choose from Most Popular</h2>
                <p>
                   {{-- Cum doctus civibus efficiantur in imperdiet deterruisset.--}}
                </p>
            </div>


            <div class="row">
                @php($i=1)
                @foreach($restaurants as $restaurant)

                        <div class="col-md-6">
                            @if($i/2 != 0)
                        <a href="{{ url('/restaurant/restaurant-detail/'.$restaurant->restaurant_id) }}" class="strip_list">
                            <div class="ribbon_1">Popular</div>
                            <div class="desc">
                                <div class="thumb_strip">
                                    <img src="{{$restaurant->restaurant_logo}}" alt="Restaurant Logo">
                                </div>

                                <?php
                                $review = new \App\Http\Controllers\Front\RestaurantDetailsFrontController();
                                $rating = $review->restaurantRating($restaurant->restaurant_id);

                                ?>

                                <div class="rating">
                                    @for($r=0 ; $r < 5; $r++)
                                        @if ($r < $rating)
                                            <i class="icon_star voted"> </i>
                                        @else
                                            <i class="icon_star"></i>

                                        @endif
                                    @endfor
                                </div>

                                <h3>{{ $restaurant->restaurant_name }}</h3>
                                <div class="type">
                                    @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allCuisines($restaurant->id) as $data)
                                        {{ $data->cuisines_name }} /
                                    @endforeach

                                </div>
                                <div class="location">
                                    {{ $restaurant->restaurant_address }}{{-- <span class="opening">Opens at 17:00</span>--}}
                                </div>
                                <ul>
                                    <li>Delivery <?php if ($restaurant->delivery_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>
                                    <li>Take away <?php if ($restaurant->take_way_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>

                                </ul>
                            </div><!-- End desc-->
                        </a><!-- End strip_list-->

                    </div><!-- End col-md-6-->
                    @else
                        <div class="col-md-6">

                                <a href="{{ url('/restaurant/restaurant-detail/'.$restaurant->restaurant_id) }}" class="strip_list">
                                    <div class="ribbon_1">Popular</div>
                                    <div class="desc">
                                        <div class="thumb_strip">
                                            <img src="{{$restaurant->restaurant_logo}}" alt="Restaurant Logo">
                                        </div>
                                        <div class="rating">
                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i
                                                    class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                                        </div>
                                        <h3>{{ $restaurant->restaurant_name }}</h3>
                                        <div class="type">
                                            @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allCuisines($restaurant->id) as $data)
                                                {{ $data->cuisines_name }} /
                                            @endforeach

                                        </div>
                                        <div class="location">
                                            {{ $restaurant->restaurant_address }} <span class="opening">Opens at 17:00</span>
                                        </div>
                                        <ul>
                                            <li>Delivery <?php if ($restaurant->delivery_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>
                                            <li>Take away <?php if ($restaurant->take_way_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>

                                        </ul>
                                    </div><!-- End desc-->
                                </a><!-- End strip_list-->
                        </div>
                    @endif
                @endforeach
            </div><!-- End row -->

        </div><!-- End container -->
    </div><!-- End white_bg -->

    <div class="high_light">
        <div class="container">
            <h3>Choose your food from all available Restaurants</h3>
            {{--<p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>--}}
            <a href="{{ route('restaurant.list') }}">View all Restaurants</a>
        </div><!-- End container -->
    </div><!-- End hight_light -->

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('front-end')}}/img/bg_office.jpg" data-natural-width="1200"
             data-natural-height="600">
        <div class="parallax-content">
            <div class="sub_content">
                <i class="icon_mug"></i>
                <h3>We deliver food everywhere</h3>
                <p>
                    Your office, your home, your party...
                </p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End Content =============================================== -->

    <div class="container margin_60">
        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Work with Us</h2>
            <p>
                Be a member of our family
            </p>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-2">
                <a class="box_work" href="{{ route('restaurant.register') }}">
                    <img src="{{asset('front-end')}}/img/submit_restaurant.jpg" width="848" height="480" alt="" class="img-responsive">
                    <h3>Submit your Restaurant<span>Start to earn customers</span></h3>
                    <p> Quick food is a grate place to connect your customers and manage your restaurant and earn money </p>
                    <div class="btn_1">Read more</div>
                </a>
            </div>
            <div class="col-md-4">
                <a class="box_work" href="{{ route('submit.rider') }}">
                    <img src="{{asset('front-end')}}/img/delivery.jpg" width="848" height="480" alt="" class="img-responsive">
                    <h3>We are looking for a Driver<span>Start to earn money</span></h3>
                    <p>If you want to looking for a job to deliver food. Quick food is great place for you..</p>
                    <div class="btn_1">Read more</div>
                </a>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->

@endsection



@section('script')
    !-- SPECIFIC SCRIPTS -->
    <script src="{{ asset('/front-end') }}/js/morphext.min.js"></script>
    <script>
        $("#js-rotating").Morphext({
            animation: "fadeIn", // Overrides default "bounceIn"
            separator: ",", // Overrides default ","
            speed: 2300, // Overrides default 2000
            complete: function () {
                // Overrides default empty function
            }
        });
    </script>
@endsection
