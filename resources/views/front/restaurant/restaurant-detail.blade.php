@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end')}}/css/skins/square/grey.css" rel="stylesheet">


    <!-- Gallery -->
    <link href="{{ asset('front-end')}}/css/slider-pro.min.css" rel="stylesheet">

@endsection

@section('subheader')

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" data-parallax="scroll" data-image-src="@isset($subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="470">
        <div id="subheader">
            <div id="sub_content">


                <div id="thumb"><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" ><img src="{{asset($restaurant->restaurant_logo)}}" alt=""></a></div>
                <div class="rating"><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">
                        <?php
                        $review = new \App\Http\Controllers\Front\RestaurantDetailsFrontController();
                        $rating = $review->restaurantRating($restaurant->restaurant_id);

                        ?>


                        @for($r=0 ; $r < 5; $r++)
                            @if ($r < intval($rating))
                                <i class="icon_star voted"> </i>
                            @else
                                <i class="icon_star"></i>

                            @endif
                        @endfor

                     (<small><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}">Read {{ $reviews->total() }} reviews</a></small>)</a>
                </div>
                <h1><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">{{ $restaurant->restaurant_name }}</a></h1>
                <div><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;">
                        <em>
                            @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allCuisines($restaurant->id) as $cuisines)
                                {{ $cuisines->cuisines_name }} /
                            @endforeach
                        </em></a>
                </div>
                <div><a href="/restaurant/restaurant-detail/{{ $restaurant->restaurant_id }}" style="text-decoration: inherit;"><i class="icon_pin"></i> {{$restaurant->restaurant_address}} - <strong>Delivery charge:</strong> ৳ {{$restaurant->delivery_fee}}</a></div>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('restaurant.list') }}">Restaurant List</a></li>
                <li><a href="{{ url('/restaurant/restaurant-menu/'.$restaurant->restaurant_id) }}">Restaurant Menu</a></li>
                <li>Restaurant Detail</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-4">
                <p>
                    <a class="btn_map"  href="{{ url('/restaurant/restaurant-menu/'.$restaurant->restaurant_id) }}" aria-expanded="false" aria-controls="collapseMap">Go Back to Menu</a>
                </p>

                <div class="box_style_2">
                    <h4 class="nomargin_top">Opening time <i class="icon_clock_alt pull-right"></i></h4>
                    <ul class="opening_list">
                        @if($restaurantService)

                        <li>Saturday<?php if ( $restaurantService->saturday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->saturday</span>";?></li>
                        <li>Sunday<?php if ( $restaurantService->sunday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->sunday</span>";?></li>
                        <li>Monday<?php if ( $restaurantService->monday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->monday</span>";?></li>
                        <li>Tuesday<?php if ( $restaurantService->tuesday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->tuesday</span>";?></li>
                        <li>Wednesday<?php if ( $restaurantService->wednesday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->wednesday</span>";?></li>
                        <li>Thursday<?php if ( $restaurantService->thursday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->thursday</span>";?></li>
                        <li>Friday<?php if ( $restaurantService->friday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->friday</span>";?></li>
                            @endif

                    </ul>
                </div>
                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="#" class="phone"> {{ $restaurant->customer_care_number }}</a>
                    <small>{{ $restaurant->customer_cate_service_duration }}</small>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box_style_2">
                    <h2 class="inner">Description</h2>

                    <div id="Img_carousel" class="slider-pro">
                        <div class="sp-slides">

                            @foreach($sliderImages as $sliderImage)

                                <div class="sp-slide">
                                    <img alt="" class="sp-image" src="../src/css/images/blank.html"
                                         data-src="{{ asset($sliderImage->sub_image)}}"
                                         data-small="{{ asset($sliderImage->sub_image)}}"
                                         data-medium="{{ asset($sliderImage->sub_image)}}"
                                         data-large="{{ asset($sliderImage->sub_image)}}"
                                         data-retina="{{ asset($sliderImage->sub_image)}}">
                                </div>

                            @endforeach

                        </div>



                        <div class="sp-thumbnails">

                            @foreach($sliderImages as $sliderImage)

                                <img alt="" class="sp-thumbnail" src="{{ asset($sliderImage->sub_image)}}">

                            @endforeach
                        </div>


                    </div>
                    <h3>About us</h3>
                    <p>
                        <?php echo $restaurant->restaurant_description?>
                    </p>

                    <div id="summary_review">
                        <div id="general_rating">
                            <?php

                                $review = new \App\Http\Controllers\Front\RestaurantDetailsFrontController();
                                $rating = $review->restaurantRating($restaurant->restaurant_id);

                            ?>
                             {{ $reviews->count() }}  Reviews
                            <div class="rating">
                            @for($r=0 ; $r < 5; $r++)
                                    @if ($r < intval($rating))
                                        <i class="icon_star voted"> </i>
                                    @else
                                        <i class="icon_star"></i>

                                    @endif
                                @endfor

                            </div>

                        </div>

                        <div class="row" id="rating_summary">
                            <div class="col-md-6">
                                <ul>
                                    <li>Food Quality
                                        <div class="rating">
                                            @for($r=0 ; $r < 5; $r++)
                                                @if ($r < intval($otherRating['foodReview']))
                                                    <i class="icon_star voted"> </i>
                                                @else
                                                    <i class="icon_star"></i>

                                                @endif
                                            @endfor

                                        </div>
                                    </li>
                                    <li>Price
                                        <div class="rating">
                                            @for($r=0 ; $r < 5; $r++)
                                                @if ($r < intval($otherRating['priceReview']))
                                                    <i class="icon_star voted"> </i>
                                                @else
                                                    <i class="icon_star"></i>

                                                @endif
                                            @endfor

                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li>Punctuality
                                        <div class="rating">
                                            @for($r=0 ; $r < 5; $r++)
                                                @if ($r < intval($otherRating['punctualityReview']))
                                                    <i class="icon_star voted"> </i>
                                                @else
                                                    <i class="icon_star"></i>

                                                @endif
                                            @endfor

                                        </div>
                                    </li>
                                    <li>Courtesy
                                        <div class="rating">
                                            @for($r=0 ; $r < 5; $r++)
                                                @if ($r < intval($otherRating['courtesyReview']))
                                                    <i class="icon_star voted"> </i>
                                                @else
                                                    <i class="icon_star"></i>

                                                @endif
                                            @endfor

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- End row -->
                        <hr class="styled">

                        @if($message = Session::get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif

                        <div class="row">



                            <form method="post" action="{{ url('/user/review') }}"  >

                                {{csrf_field()}}
                                <input name="restaurant_unique_id"  type="hidden" value="{{ $restaurant->restaurant_id }}">
                                <input name="user_id" type="hidden" value="<?php if (Auth::guard('web')->check()) {echo Auth::user()->id;} else { echo '';} ?>">

                                <div class="row">

                                       {{-- <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('name_review') ? ' has-error' : '' }}" >
                                                <input name="name_review" id="name_review" type="text" required placeholder="Name" class="form-control ">
                                                @if ($errors->has('name_review'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_review') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                        </div>

                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('email_review') ? ' has-error' : '' }}">
                                            <input name="email_review" id="email_review" type="text" placeholder="Email" required class="form-control ">
                                            @if ($errors->has('email_review'))
                                                <span class="help-block">
                                                   <strong>{{ $errors->first('email_review') }}</strong>
                                                 </span>
                                            @endif
                                        </div>

                                    </div>--}}


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control " name="food_review" required id="food_review">
                                                <option value="">Food Quality</option>
                                                <option value="1">Low</option>
                                                <option value="2">Sufficient</option>
                                                <option value="3">Good</option>
                                                <option value="4">Excellent</option>
                                                <option value="5">Super</option>

                                            </select>
                                            @if ($errors->has('food_review'))
                                                <span class="help-block">
                                                   <strong>{{ $errors->first('food_review') }}</strong>
                                                 </span>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control "  name="price_review" required id="price_review">
                                                <option value="">Price</option>
                                                <option value="1">Low</option>
                                                <option value="2">Sufficient</option>
                                                <option value="3">Good</option>
                                                <option value="4">Excellent</option>
                                                <option value="5">Super</option>


                                            </select>
                                            @if ($errors->has('price_review'))
                                                <span class="help-block">
                                                   <strong>{{ $errors->first('price_review') }}</strong>
                                                 </span>
                                            @endif
                                        </div>

                                    </div>
                                </div><!--End Row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control "  name="punctuality_review" required id="punctuality_review">
                                                <option value="">Punctuality</option>
                                                <option value="1">Low</option>
                                                <option value="2">Sufficient</option>
                                                <option value="3">Good</option>
                                                <option value="4">Excellent</option>
                                                <option value="5">Super</option>

                                            </select>

                                            @if ($errors->has('punctuality_review'))
                                                <span class="help-block">
                                                   <strong>{{ $errors->first('punctuality_review') }}</strong>
                                                 </span>
                                            @endif
                                        </div>
                                      </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control"  name="courtesy_review" required id="courtesy_review">
                                                <option value="">Courtesy</option>
                                                <option value="1">Low</option>
                                                <option value="2">Sufficient</option>
                                                <option value="3">Good</option>
                                                <option value="4">Excellent</option>
                                                <option value="5">Super</option>

                                            </select>

                                            @if ($errors->has('courtesy_review'))
                                                <span class="help-block">
                                                   <strong>{{ $errors->first('courtesy_review') }}</strong>
                                                 </span>
                                            @endif
                                        </div>

                                    </div>
                                </div><!--End Row -->
                                <textarea name="review_text" id="review_text" class="form-control " style="height:100px" placeholder="Write your review"></textarea>
                                @if(\Illuminate\Support\Facades\Auth::guard('web')->check())
                                    <button type="submit"   class="btn btn-submit" >Submit</button>
                                @else
                                    <button  onclick="myFunction()"    class="btn btn-submit" >Submit</button>
                                    <script>
                                        function myFunction() {
                                            alert("Please Login First !! ");
                                            window.location.href = "{{ url('/restaurant/restaurant-detail/'.$restaurant->restaurant_id) }}";
                                        }
                                    </script>
                                @endif


                            </form>


                        </div><!-- End row  -->


                    </div><!-- End summary_review -->





                    @foreach($reviews as $review)
                        <div class="review_strip_single last">
                        <img src="{{ asset('front-end')}}/img/avatar2.jpg" alt="" class="img-circle">
                        <small> -  @foreach(\App\Http\Controllers\Front\RestaurantDetailsFrontController::reviewUser($restaurant->id) as $restaurantUser)
                                  <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($restaurantUser->created_at))->diffForHumans(); ?>
                            @endforeach -</small>
                        <h4>
                            @foreach(\App\Http\Controllers\Front\RestaurantDetailsFrontController::reviewUser($restaurant->id) as $restaurantUser)
                                {{ $restaurantUser->first_name }}
                            @endforeach
                        </h4>
                        <p>
                            @foreach(\App\Http\Controllers\Front\RestaurantDetailsFrontController::reviewUser($restaurant->id) as $restaurantUser)
                                {{ $restaurantUser->review_text }}
                            @endforeach
                        </p>
                        <div class="row">
                            <div class="col-md-3">

                                <div class="rating">
                                    @for($i=0; $i < $review->food_review; $i++ )
                                         <i class="icon_star voted"></i>
                                    @endfor
                                </div>
                                Food Quality
                            </div>

                            <div class="col-md-3">
                                <div class="rating">
                                    @for($i=0; $i < $review->price_review; $i++ )
                                        <i class="icon_star voted"></i>
                                    @endfor
                                </div>
                                Price
                            </div>
                            <div class="col-md-3">
                                <div class="rating">
                                    @for($i=0; $i < $review->punctuality_review; $i++ )
                                        <i class="icon_star voted"></i>
                                    @endfor
                                </div>
                                Punctuality
                            </div>
                            <div class="col-md-3">
                                <div class="rating">
                                    @for($i=0; $i < $review->courtesy_review; $i++ )
                                        <i class="icon_star voted"></i>
                                    @endfor
                                </div>
                                Courtesy
                            </div>
                        </div><!-- End row -->
                    </div><!-- End review strip -->
                    @endforeach

                </div><!-- End box_style_1 -->

                {{ $reviews->links() }}
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->







@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')

    <!-- SPECIFIC SCRIPTS -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAs_JyKE9YfYLSQujbyFToZwZy-wc09w7s"></script>
    <script src="{{ asset('front-end')}}/js/map_single.js"></script>
    <script src="{{ asset('front-end')}}/js/infobox.js"></script>
    <script src="{{ asset('front-end')}}/js/jquery.sliderPro.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function( $ ) {
            $( '#Img_carousel' ).sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                smallSize: 500,
                startSlide: 0,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });
        });
    </script>

@endsection
