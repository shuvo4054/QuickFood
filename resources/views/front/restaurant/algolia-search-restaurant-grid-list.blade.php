@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset($subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>{{ $restaurants->total() }} Available Restaurants </h1>

                <form method="POST" action="{{ route('search') }}">
                    {{ csrf_field() }}

                    <div id="custom-search-input">
                        <div class="input-group ">
                            <input type="text" name="search_key" class="search-query" placeholder="Search Restaurant by Name, City, Address..">
                            <span class="input-group-btn">
                        <input type="submit" class="btn_search" value="submit">
                        </span>
                        </div>
                    </div>

                </form>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Restaurant List</li>
            </ul>

        </div>
    </div><!-- Position -->



    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">

                <div id="filters_col">
                    <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters <i class="icon-plus-1 pull-right"></i></a>
                    <div class="collapse" id="collapseFilters">
                        <div class="filter_type">

                            <h6>City</h6>
                            <ul>
                                <li><label><input type="checkbox"  class="icheck"><a href="{{url('/search-city/all')}}">All </a></label></li>
                                @php($cityCoutn=1)
                                @foreach($cities as $city)
                                    <li><label><input type="checkbox" class="icheck"><a href="{{url('/search-city/'.$city->city_name) }}">{{ $city->city_name }} </a></label><i class="color_{{$cityCoutn++}}"></i></li>

                                @endforeach
                            </ul>
                        </div>
                        <div class="filter_type">

                            <h6>Rating</h6>
                            <ul>
                                <li>
                                    <label>
                                        <input type="checkbox" class="icheck">
                                        <span class="rating">
                                            <a href="{{url('/search-service/rating/5')}}">
							               5 stars(<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i>) </a>
                                        </span>
                                    </label>
                                </li>

                                <li>
                                    <label>
                                        <input type="checkbox" class="icheck">
                                        <span class="rating">
                                            <a href="{{url('/search-service/rating/4')}}">
							               4 stars(<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i></i><i class="icon_star voted"></i>) </a>
                                        </span>
                                    </label>
                                </li>

                                <li>
                                    <label>


                                        <input type="checkbox" class="icheck">

                                        <span class="rating">
                                            <a href="{{url('/search-service/rating/3')}}">
							               3 stars(<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i>) </a>
                                        </span>

                                    </label>
                                </li>

                                <li>
                                    <label>


                                        <input type="checkbox" class="icheck">

                                        <span class="rating">
                                            <a href="{{url('/search-service/rating/2')}}">
							               2 stars(<i class="icon_star voted"></i><i class="icon_star voted"></i>) </a>
                                        </span>

                                    </label>
                                </li>

                                <li>
                                    <label>


                                        <input type="checkbox" class="icheck">

                                        <span class="rating">
                                            <a href="{{url('/search-service/rating/1')}}">
							               1 star(<i class="icon_star voted"></i>) </a>
                                        </span>

                                    </label>
                                </li>
                            </ul>

                        </div>
                        <div class="filter_type">
                            <h6>Options</h6>
                            <ul class="nomargin">


                                <li><label><input type="checkbox" class="icheck"><a href="{{url('/search-service/delivery')}}">Delivery </a></label></li>
                                <li><label><input type="checkbox" class="icheck"><a href="{{ url('/search-service/takeway') }}">Take Away </a></label></li>

                            </ul>
                        </div>
                    </div><!--End collapse -->
                </div><!--End filters col-->
            </div><!--End col-md -->

            <div class="col-md-9">

                <div id="tools">

                </div><!--End tools -->
                @if( $restaurants->total() > 0)

                    <div class="row">


                        <?php $delay= 0.1; $i=1?>
                        @foreach($restaurants as $restaurant)
                            @if($restaurant->verify == 1)

                            <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="{{$delay=$delay+0.2}}s">
                                <a class="strip_list grid" href="{{ url('restaurant/restaurant-menu/'.$restaurant->restaurant_id) }}">
                                    @if(! $restaurant->popularity == 0)
                                        <div class="ribbon_1">Popular</div>
                                    @endif
                                    <div class="desc">
                                        <div class="thumb_strip">
                                            <img src="{{asset($restaurant->restaurant_logo)}}" alt="">
                                        </div>


                                        <?php
                                        $review = new \App\Http\Controllers\Front\RestaurantDetailsFrontController();
                                        $rating = $review->restaurantRating($restaurant->restaurant_id);

                                        ?>

                                        <div class="rating">
                                            @for($r=0 ; $r < 5; $r++)
                                                @if ($r < $rating)
                                                    <i class="icon_star voted"> </i>
                                                @else
                                                    <i class="icon_star"></i>

                                                @endif
                                            @endfor
                                        </div>
                                        <h3>{{ $restaurant->restaurant_name }}</h3>
                                        <div class="type">

                                            @foreach(\App\Http\Controllers\Front\RestaurantFrontController::allCuisines($restaurant->id) as $data)
                                                {{ $data->cuisines_name }} /
                                            @endforeach

                                        </div>
                                        <div class="location">
                                            {{ $restaurant->restaurant_address }} <br><span class="opening">Minimum order: ৳{{ $restaurant->minimum_order }}</span>
                                        </div>
                                        <ul>
                                            <li>Delivery <?php if ($restaurant->delivery_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>
                                            <li>Take away <?php if ($restaurant->take_way_service == 'yes'){ echo '<i class="icon_check_alt2 ok"></i>';} else{echo '<i class="icon_close_alt2 no"></i>';} ?></li>

                                        </ul>
                                    </div>
                                </a><!-- End strip_list-->
                            </div><!-- End col-md-6-->


                            @endif


                        @endforeach

                    </div><!-- End row-->
                    <div class="load_more_bt wow fadeIn">
                        {{ $restaurants->links() }}
                    </div>

                @else

                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h3 style="text-align: center">No Restaurants Found</h3>
                    </div>


                @endif


            </div><!-- End col-md-9-->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection





@section('script')

    <!-- SPECIFIC SCRIPTS -->
    <script  src="{{asset('front-end')}}/js/cat_nav_mobile.js"></script>
    <script>$('#cat_nav').mobileMenu();</script>
    <script src="{{asset('front-end')}}/js/ion.rangeSlider.js"></script>
    <script>
        $(function () {
            'use strict';
            $("#range").ionRangeSlider({
                hide_min_max: true,
                keyboard: true,
                min: 0,
                max: 15,
                from: 0,
                to:5,
                type: 'double',
                step: 1,
                prefix: "Km ",
                grid: true
            });
        });
    </script>
@endsection
