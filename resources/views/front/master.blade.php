<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>QuickFood - Quality delivery or take away food</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('front-end')}}/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('front-end')}}/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('front-end')}}/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('front-end')}}/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('front-end')}}/img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic'
          rel='stylesheet' type='text/css'>

    <!-- BASE CSS -->
    <link href="{{asset('front-end')}}/css/base.css" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{asset('front-end')}}/css/morphext.css" rel="stylesheet">


    @yield('link')


    <!--[if lt IE 9]>
    <script src="{{asset('front-end')}}/js/html5shiv.min.js"></script>
    <script src="{{asset('front-end')}}/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!--[if lte IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a>.</p>
<![endif]-->

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave" id="status">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div><!-- End Preload -->

@include('front.includes.header')

@yield('subheader')

@yield('body')

@include('front.includes.footer')



@yield('search-menu')

<!-- COMMON SCRIPTS -->
<script src="{{asset('front-end')}}/js/jquery-2.2.4.min.js"></script>
<script src="{{asset('front-end')}}/js/common_scripts_min.js"></script>
<script src="{{asset('front-end')}}/js/functions.js"></script>
<script src="{{asset('front-end')}}/assets/validate.js"></script>


@yield('script')

</body>
</html>
