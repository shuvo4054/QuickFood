<!-- Footer ================================================== -->
<footer>
    <div class="container">
        <div class="row">

           {{-- <div class="col-md-4 col-sm-3">
                <h3>Secure payments with</h3>
                <i class="fa fa-money" style="font-size:48px;color:red"></i>
                <p>
                    <img src="{{asset('front-end')}}/img/cards.png" alt="" class="img-responsive">
                </p>
            </div>--}}



            <div class="col-md-3 col-sm-3 col-md-offset-3">
                <h3>About</h3>
                <ul>
                    <li><a href="{{ route('aboutUs') }}">About us</a></li>
                    <li><a href="{{ route('faq') }}">Faq</a></li>
                    <li><a href="#">Contacts</a></li>
                    {{--<li><a href="#0" data-toggle="modal" data-target="#login_2">Login</a></li>
                    <li><a href="#0" data-toggle="modal" data-target="#register">Register</a></li>
              --}}{{--      <li><a href="#0">Terms and conditions</a></li>--}}
                </ul>
            </div>

           {{-- <div class="col-md-3 col-sm-3" id="newsletter">
                <h3>Newsletter</h3>
                <p>
                    Join our newsletter to keep be informed about offers and news.
                </p>
                <div id="message-newsletter_2">
                </div>
                <form method="post" action="http://www.ansonika.com/quickfood/assets/newsletter.php" name="newsletter_2"
                      id="newsletter_2">
                    <div class="form-group">
                        <input name="email_newsletter_2" id="email_newsletter_2" type="email" value=""
                               placeholder="Your mail" class="form-control">
                    </div>
                    <input type="submit" value="Subscribe" class="btn_1" id="submit-newsletter_2">
                </form>
            </div>--}}

            <div class="col-md-2 col-sm-3">
                <h3>Settings</h3>
                <div class="styled-select">
                    <select class="form-control" name="lang" id="lang">
                        <option value="English" selected>English</option>
                       {{-- <option value="French">French</option>
                        <option value="Spanish">Spanish</option>
                        <option value="Russian">Russian</option>--}}
                    </select>
                </div>
                <div class="styled-select">
                    <select class="form-control" name="currency" id="currency">
                        <option value="USD" selected>BDT</option>
                        {{--<option value="EUR">EUR</option>
                        <option value="GBP">GBP</option>
                        <option value="RUB">RUB</option>--}}
                    </select>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <li><a href="https://www.facebook.com"><i class="icon-facebook"></i></a></li>
                        <li><a href="http://twitter.com"><i class="icon-twitter"></i></a></li>
                        <li><a href="https://plus.google.com"><i class="icon-google"></i></a></li>
                        <li><a href="https://www.instagram.com"><i class="icon-instagram"></i></a></li>
                      {{--  <li><a href="#0"><i class="icon-pinterest"></i></a></li>
                        <li><a href="#0"><i class="icon-vimeo"></i></a></li>--}}
                        <li><a href="https://www.youtube.com"><i class="icon-youtube-play"></i></a></li>
                    </ul>
                    <p>
                        &copy; Quick Food <?php echo date("Y");?>
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
</footer>
<!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->


