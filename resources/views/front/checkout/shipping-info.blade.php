@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/date_time_picker.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_2.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_3.html" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('restaurant.list') }}">Restaurant List</a></li>
                <li><a href="{{ url('/restaurant/restaurant-menu/'.Session::get('restaurant_unique_id')) }}">Restaurant Menu</a></li>
                <li><a href="{{ url('/show-cart') }}">Order List</a></li>
                <li>Your Details</li>
            </ul>


        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-3">

                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        We deliver yor order in time. pri partem essent. Qui debitis meliore ex, tollit debitis
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.
                    </p>
                </div><!-- End box_style_1 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <?php
                        $restaurant = new \App\Http\Controllers\Front\RestaurantFrontController();
                        $restaurant = $restaurant->restaurantServiceTime();
                    ?>
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="#" class="phone"> {{ $restaurant->customer_care_number }}</a>
                    <small>{{ $restaurant->customer_cate_service_duration }}</small>
                </div>

            </div><!-- End col-md-3 -->

            <div class="col-md-6">
                <div class="box_style_2" id="order_process">
                    <form id="shipping_info" action="{{ url('/new-shipping') }}" method="POST">
                        {{ csrf_field() }}
                        <h2 class="inner">Your order details</h2>


                        <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
                            <label for="full_name">Name</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" value="{{ $customer->first_name.' '.$customer->last_name }}" placeholder="Full Name"  required autofocus>

                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $customer->email }}" placeholder="Email"  required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                            <label for="mobile_number">Email</label>
                            <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{ $customer->mobile_number }}" placeholder="Mobile Number"  required autofocus>

                            @if ($errors->has('mobile_number'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ $customer->city }}" placeholder="City"  required autofocus>

                            @if ($errors->has('city'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class="form-group {{ $errors->has('living_address') ? ' has-error' : '' }}">
                            <label for="living_address">Livid Address</label>
                            <input type="text" id="living_address" name="living_address" class="form-control" placeholder="80, Vila, Dhaka-1215, Dhanmondi" value="{{ $customer->living_address }}" required>

                            @if ($errors->has('living_address'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('living_address') }}</strong>
                                    </span>
                            @endif

                        </div>





                        <hr>
                        <div class="row">

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('delivery_schedule_day') ? ' has-error' : '' }}">
                                    <label for="delivery_schedule_day"><i class="icon-calendar-7"></i> Select a date</label>
                                    <input class="form-control" id="delivery_schedule_day" name="delivery_schedule_day" type="date" required>
                                    @if ($errors->has('delivery_schedule_day'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('delivery_schedule_day') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="delivery_schedule_time">Delivery time</label>
                                    <select class="form-control" name="delivery_schedule_time" id="delivery_schedule_time" required>
                                        <option value="" selected>Select time</option>
                                        <?php
                                            $deliveryTimes = new \App\Http\Controllers\Front\RestaurantFrontController();
                                            $deliveryTimes = $deliveryTimes->deliveryTime();
                                        ?>
                                        @foreach($deliveryTimes as $deliveryTime)

                                         <option value="{{ $deliveryTime->time }}">{{ $deliveryTime->time }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row" id="options_2">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <label><input type="radio" value="delivery"  name="option_2" class="icheck">Delivery</label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <label><input type="radio" value="takeway" name="option_2" class="icheck">Take Away</label>
                            </div>
                            @if ($errors->has('option_2'))
                                <span class="help-block">
                                        <strong class="text text-danger">{{ $errors->first('option_2') }}</strong>
                                    </span>
                            @endif
                        </div><!-- Edn options 2 -->




                    </form>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box">
                        <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                        <table class="table table_summary">
                            <tbody>
                            @php($sum=0)

                            @foreach($cartProducts as $cartProduct)
                                <tr>
                                    <td>
                                        <a href="" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>{{ $cartProduct->qty }}x</strong> {{ $cartProduct->name }}
                                    </td>
                                    <td>
                                        <strong class="pull-right">৳ {{ $cartProduct->price }}</strong>
                                    </td>
                                </tr>
                                <?php $total = $cartProduct->price*$cartProduct->qty  ?>
                                @php($sum = $sum + $total)
                            @endforeach

                            </tbody>

                        </table>

                        <hr>
                        <table class="table table_summary">
                            <tbody>
                            <tr>
                                <td>
                                    Subtotal <span class="pull-right">৳ {{ $sum }}</span>
                                </td>
                            </tr>
                            {{--<tr>
                                <td>
                                    Delivery fee <span class="pull-right">৳ {{ Session::get('delivery_fee')}}</span>
                                </td>
                            </tr>--}}
                            <tr>
                                <td class="total">
                                    Total <span class="pull-right">৳ {{ Session::get('grandTotal') }}</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <button class="btn_full" type="submit" form="shipping_info">Go to checkout</button>
                        <a class="btn_full_outline" href="{{ url('/restaurant/restaurant-menu/'.Session::get('restaurant_unique_id')) }}"><i class="icon-right"></i> Add other items</a>
                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection



@section('script')



    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('front-end')}}/js/theia-sticky-sidebar.js"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>

    <!-- Date and time pickers -->
    <script src="{{asset('front-end')}}/js/bootstrap-datepicker.js"></script>
    <script src="{{asset('front-end')}}/js/bootstrap-timepicker.js"></script>
    <script>
        $('input.date-pick').datepicker('setDate', 'today');
        $('input.time-pick').timepicker({
            minuteStep: 15,
            showInpunts: false
        })
    </script>




@endsection
