@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">


@endsection
<!-- SubHeader =============================================== -->
@section('subheader')
<?php
$subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
$subHeaderImage = $subHeaderImage->subHeaderImage();
?>
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">
<div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="cart_3.html" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ route('restaurant.list') }}">Restaurant List</a></li>
              {{--  <li><a href="{{ url('/restaurant/restaurant-menu/'.Session::get('restaurant_unique_id')) }}">Restaurant Menu</a></li>
                <li><a href="{{ url('/show-cart') }}">Order List</a></li>--}}
                <li><a href="{{ url('/shipping-info') }}">Your Details</a></li>
                <li>Payment</li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-3">

                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        We deliver yor order in time. Whatever it takes. So connect with us.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        Pay your order by Cash. It's a very secure to pay. For any problem contact with us in our customer care number.
                    </p>
                </div><!-- End box_style_1 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <?php
                    $restaurant = new \App\Http\Controllers\Front\RestaurantFrontController();
                    $restaurant = $restaurant->restaurantServiceTime();
                    ?>
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="#" class="phone"> {{ $restaurant->customer_care_number }}</a>
                    <small>{{ $restaurant->customer_cate_service_duration }}</small>
                </div>

            </div><!-- End col-md-3 -->

            <div class="col-md-6">
                <div class="box_style_2">

                    <form id="new_shipping" action="{{ url('/new-order') }}" method="POST">
                        {{ csrf_field() }}
                        <h2 class="inner">Payment methods</h2>



                      {{--  <div class="payment_select" id="paypal">
                            <label><input type="radio" value="BKash" name="payment_type" class="icheck">Pay with BKash</label>
                        </div>--}}

                        <div class="payment_select nomargin">
                            <label><input type="radio" checked value="Cash On Delivery"  name="payment_type" class="icheck">Pay with cash</label>
                            <i class="icon_wallet"></i>
                        </div>
                    </form>

                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

                   <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box">
                        <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                        <table class="table table_summary">
                            <tbody>
                            @php($sum=0)

                            @foreach($cartProducts as $cartProduct)
                                <tr>
                                    <td>
                                        <a href="" class="remove_item"><i class="icon_minus_alt"></i></a> <strong>{{ $cartProduct->qty }}x</strong> {{ $cartProduct->name }}
                                    </td>
                                    <td>
                                        <strong class="pull-right">৳ {{ $cartProduct->price }}</strong>
                                    </td>
                                </tr>
                                <?php $total = $cartProduct->price*$cartProduct->qty  ?>
                                @php($sum = $sum + $total)
                            @endforeach

                            </tbody>

                        </table>

                        <hr>
                        <table class="table table_summary">
                            <tbody>
                            <tr>
                                <td>
                                    Subtotal <span class="pull-right">৳ {{ $sum }}</span>
                                </td>
                            </tr>

                            <?php
                                $serviceType = new \App\Http\Controllers\Front\RestaurantFrontController();
                                $serviceType = $serviceType->deliveryType();
                            ?>

                            @if($serviceType->service_type == 'delivery')

                            <tr>
                                <td>
                                    Delivery fee <span class="pull-right">৳ {{ Session::get('delivery_fee')}}</span>
                                </td>
                            </tr>

                            @endif
                            <tr>
                                <td class="total">


                                    @if($serviceType->service_type == 'delivery')

                                        Total <span class="pull-right">৳ {{ $grandTotal = (Session::get('delivery_fee') + Session::get('grandTotal')) }}</span>
                                        {{ Session::put('grandTotal', $grandTotal) }}
                                    @else
                                        Total <span class="pull-right">৳ {{ Session::get('grandTotal') }}</span>
                                    @endif

                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <button class="btn_full" type="submit" form="new_shipping">Go to checkout</button>

                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

@endsection





@section('script')




    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('front-end')}}/js/theia-sticky-sidebar.js"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>

@endsection
