@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>Login or register to continue with checkout</h1>
                <div>{{--<i class="icon_pin"></i> 135 Newtownards Road, Belfast, BT4 1AB--}}</div>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>

                <li>Checkout/li>
            </ul>

        </div>
    </div><!-- Position -->


    <!-- Content ================================================== -->






    <div class="white_bg">
            <div class="container margin_60_35">
                <div class="main_title margin_mobile">

                    <h3 class="nomargin_top">Login form here</h3>

                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        @if($message = Session::get('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ url('/customer-login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <label for="remember">
                                        <input id="remember" name="remember"  {{ old('remember') ? 'checked' : '' }} type="checkbox" class="icheck">Remember Me
                                    </label>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <button type="submit" class="btn_full_outline">
                                                Login
                                            </button>
                                        </div>

                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Forgot Your Password?
                                        </a>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div><!-- End col  -->
                </div><!-- End row  -->
            </div>
        </div>



    <div class="container margin_60_35">
                <div class="main_title margin_mobile">
                    <h3 class="nomargin_top">Sing in form here</h3>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form  method="POST" action="{{  url('/new-customer') }}">
                            {{ csrf_field() }}

                            <div class="row">


                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name">First name</label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Mahabub Jahan" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name">Last name</label>
                                        <input type="text" class="form-control" id="name_contact" name="last_name" placeholder="Shuvo" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <div class="row">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">Email:</label>
                                        <input type="email" id="email" name="email" class="form-control " placeholder="shuvo@email.com" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>



                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                        <label for="mobile_number">Mobile number:</label>
                                        <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="01722222222" value="{{ old('mobile_number') }}" required>
                                    </div>
                                    @if ($errors->has('mobile_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>


                            <div class="row">



                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label for="city">Address</label>
                                        <input type="text" id="city" name="city" class="form-control" placeholder="Dhaka" value="{{ old('city') }}" required>

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('living_address') ? ' has-error' : '' }}">
                                        <label for="living_address">Livid Address</label>
                                        <input type="text" id="living_address" name="living_address" class="form-control" placeholder="80, Vila, Dhaka-1215, Dhanmondi" value="{{ old('living_address') }}" required>

                                        @if ($errors->has('living_address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('living_address') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                </div>
                            </div><!-- End row  -->



                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">Create a password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Password"  id="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm password</label>
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password"  id="password_confirmation">
                                    </div>
                                </div>
                            </div><!-- End row  -->


                            <div id="pass-info" class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label><input name="mobile" type="checkbox" value="" class="icheck" checked>Accept <a href="#0">terms and conditions</a>.</label>
                                </div>
                            </div><!-- End row  -->
                            <hr style="border-color:#ddd;">
                            <div class="text-center">
                                <button class="btn_full_outline" type="submit">Submit</button>
                            </div>
                        </form>
                    </div><!-- End col  -->
                </div><!-- End row  -->
            </div><!-- End container  -->



    <!-- End Content =============================================== -->

@endsection






@section('script')

    <!-- SPECIFIC SCRIPTS -->
    <script src="{{asset('front-end')}}/js/cat_nav_mobile.js"></script>
    <script>$('#cat_nav').mobileMenu();</script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAs_JyKE9YfYLSQujbyFToZwZy-wc09w7s"></script>
    <script src="{{asset('front-end')}}/js/map.js"></script>
    <script src="{{asset('front-end')}}/js/infobox.js"></script>
    <script src="{{asset('front-end')}}/js/ion.rangeSlider.js"></script>
    <script>
        $(function () {
            'use strict';
            $("#range").ionRangeSlider({
                hide_min_max: true,
                keyboard: true,
                min: 0,
                max: 15,
                from: 0,
                to: 5,
                type: 'double',
                step: 1,
                prefix: "Km ",
                grid: true
            });
        });
    </script>
@endsection
