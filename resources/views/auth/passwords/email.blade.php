@extends('front.master')

@section('link')
    <!-- Radio and check inputs -->
    <link href="{{asset('front-end')}}/css/skins/square/grey.css" rel="stylesheet">

@endsection

@section('subheader')



    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

        <div id="subheader">
            <div id="sub_content">
                <h1>User Reset Password</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>User Reset Password</li>
            </ul>

        </div>
    </div><!-- Position -->




    <div class="container margin_60_35">
        <div class="main_title margin_mobile">
            <h2 class="nomargin_top">Reset Password</h2>

        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif


                <form class="form-horizontal" method="POST" action="{{ route('restaurant.password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- End col  -->
        </div><!-- End row  -->
    </div><!-- End container  -->



    <!-- End Content =============================================== -->
@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')

@endsection







