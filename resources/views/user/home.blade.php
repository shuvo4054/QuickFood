@extends('front.master')

@section('link')
    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/jvectormap/jquery-jvectormap.css">


@endsection

@section('subheader')



    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>My Order</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>My Order</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">

                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('user.orders') }}" class="active">My Orders </a></li>
                        <li><a href="{{ route('user.profile') }}" >Profile </a></li>
                        <li><a href="{{ route('user.emailPassword') }}" >Change Email & Password </a></li>
                        <li> <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    </ul>
                </div><!-- End box_style_1 -->


            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>My Order </span></a>
                            </li>

                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">

                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>SL No</th>
                                        <th>Order Id</th>
                                        <th>Restaurant</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                  @php($i=1)
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $order->id }}</td>
                                            <td>
                                            @foreach( \App\Http\Controllers\HomeController::restaurantName($order->restaurant_unique_id)  as $restaurant)
                                                {{ $restaurant->restaurant_name }}
                                                @break
                                            @endforeach
                                            </td>



                                            <td>{{ $order->order_total }}</td>
                                            <td>

                                                @foreach(\App\Http\Controllers\HomeController::shipping($order->shipping_id) as $shipping)
                                                  {{ $shipping->delivery_schedule_day }}
                                                @endforeach





                                            </td>



                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection

@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>







    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection