@extends('front.master')

@section('link')
    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">


@endsection

@section('subheader')



    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>My Profile</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Profile</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">

                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('user.orders') }}">My Orders </a></li>
                        <li><a href="{{ route('user.profile') }}" class="active">Profile </a></li>
                        <li><a href="{{ route('user.emailPassword') }}">Email & Password </a></li>
                        <li> <a href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    </ul>
                </div><!-- End box_style_1 -->

            </div><!-- End col-md-3 -->

            <div class="col-md-9">
                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Edit Profile </span></a>
                            </li>

                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <div class="indent_title_in">
                                <i class="icon_house_alt"></i>
                                <h3>My Profile</h3>

                            </div>

                            <div class="wrapper_indent">



                                <form method="POST" action="{{ url('profile/'.$user->last_name) }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name">Your First Name</label>
                                        <input class="form-control" name="first_name"
                                               value="{{ $user->first_name }}" id="first_name" type="text">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name">Your Last Name</label>
                                        <input class="form-control" name="last_name"
                                               value="{{ $user->last_name }}" id="last_name" type="text">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>





                                    <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                        <label for="mobile_number">Your Mobile Number</label>
                                        <input class="form-control" name="mobile_number"
                                               value="{{ $user->mobile_number }}" id="mobile_number"
                                               type="text">
                                        @if ($errors->has('mobile_number'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label for="city">City</label>
                                        <input class="form-control" name="city"
                                               value="{{ $user->city }}" id="city" type="text">

                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif


                                    </div>

                                    <div class="form-group{{ $errors->has('living_address') ? ' has-error' : '' }}">
                                        <label for="living_address">Living Address</label>
                                        <input class="form-control" name="living_address"
                                               value="{{ $user->living_address }}" id="living_address" type="text">

                                        @if ($errors->has('living_address'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('living_address') }}</strong>
                                    </span>
                                        @endif


                                    </div>


                                    <hr class="styled_2">
                                    <div class="wrapper_indent">
                                        <button type="submit" class="btn_1">Save now</button>
                                    </div><!-- End wrapper_indent -->
                                </form>
                            </div>

                        </section><!-- End section 1 -->




                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection



@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>



@endsection
