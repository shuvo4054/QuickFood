@extends('front.master')

@section('link')
    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $subHeaderImage = new \App\Http\Controllers\Front\RestaurantFrontController();
    $subHeaderImage = $subHeaderImage->subHeaderImage();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="@isset( $subHeaderImage->sub_header_image){{asset($subHeaderImage->sub_header_image)}}@endisset" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">
            <div id="sub_content">
                <h1>Change Email and Password</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->


@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Email & Password</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">

                <div class="box_style_1">
                    <ul id="cat_nav">
                        <ul id="cat_nav">
                            <li><a href="{{ route('user.orders') }}">My Orders </a></li>
                            <li><a href="{{ route('user.profile') }}" >Profile </a></li>
                            <li><a href="{{ route('user.emailPassword') }}" class="active">Email & Password </a></li>
                            <li> <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>
                    </ul>
                </div><!-- End box_style_1 -->


            </div><!-- End col-md-3 -->

            <div class="col-md-9">
                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Update Email & Password </span></a>
                            </li>

                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <div class="row">

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_lock_alt"></i>
                                        <h3>Change your password</h3>
                                    </div>
                                    <div class="wrapper_indent">

                                        <form method="POST" action="{{ route('change.user.password') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                                <label for="old_password">Old password</label>
                                                <input class="form-control" name="old_password" id="old_password"
                                                       type="password" required autofocus>
                                                @if ($errors->has('old_password'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Create a password</label>
                                                <input type="password" class="form-control" name="password" placeholder="Password"  id="password" required autofocus>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                @endif
                                            </div>


                                            <div class="form-group">
                                                <label for="password_confirmation">Confirm password</label>
                                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password"  id="password_confirmation">
                                            </div>

                                            <button type="submit" class="btn_1 green">Update Password</button>
                                        </form>
                                    </div><!-- End wrapper_indent -->
                                </div>


                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_mail_alt"></i>
                                        <h3>Change your email</h3>

                                    </div>
                                    <div class="wrapper_indent">

                                        <form method="POST"
                                              action="{{ route('change.user.email') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('old_email') ? ' has-error' : '' }}">
                                                <label for="old_email">Old email</label>
                                                <input class="form-control" name="old_email" id="old_email"
                                                       type="email" required autofocus>
                                                @if ($errors->has('old_email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">New email</label>
                                                <input class="form-control" name="email" id="email"
                                                       type="email" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="email_confirmation">Confirm new email</label>
                                                <input class="form-control" name="email_confirmation"
                                                       id="email_confirmation"
                                                       type="email" required autofocus>
                                            </div>
                                            <button type="submit" class="btn_1 green">Update Email</button>
                                        </form>
                                    </div><!-- End wrapper_indent -->
                                </div>

                            </div><!-- End row -->






                        </section><!-- End section 1 -->




                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection

@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>

@endsection
