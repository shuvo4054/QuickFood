<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Restaurant Details
            <small>View Restaurant Details Info</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->

                    <div class="box-body">
                        <h2>Restaurant Owner Info</h2>
                        <hr/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Restaurant Owner Name</th>
                                <td>{{ $restaurantOwnerInfo->first_name  }}</td>
                            </tr>
                            <tr>
                                <th>Email Address</th>
                                <td>{{ $restaurantOwnerInfo->email }}</td>
                            </tr>
                            <tr>
                                <th>Mobile Number</th>
                                <td>{{ $restaurantOwnerInfo->mobile_number }}</td>
                            </tr>

                        </table>


                        <h2>Restaurant Information</h2>
                        <hr/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Restaurant Name</th>
                                <td>{{ $restaurantInfo->restaurant_name }}</td>
                            </tr>

                            <tr>
                                <th>City</th>
                                <td>{{ $restaurantInfo->restaurant_description }}</td>
                            </tr>

                            <tr>
                                <th>Restaurant Address</th>
                                <td>{{ $restaurantInfo->restaurant_address }}</td>
                            </tr>

                            <tr>
                                <th>Delivery Zones</th>
                                <td>{{ $restaurantInfo->delivery_zone }}</td>
                            </tr>

                            <tr>
                                <th>Restaurant Delivery Fee</th>
                                <td>{{ $restaurantInfo->delivery_fee }}</td>
                            </tr>



                            <tr>
                                <th>Restaurant Minimum Order</th>
                                <td>{{ $restaurantInfo->minimum_order }}</td>
                            </tr>

                            <tr>
                                <th>Take Way Service</th>
                                <td>{{ $restaurantInfo->take_way_service }}</td>
                            </tr>


                            <tr>
                                <th>Delivery Service</th>
                                <td>{{ $restaurantInfo->delivery_service }}</td>
                            </tr>



                            <tr>
                                <th>Restaurant Hot Line Number</th>
                                <td>{{ $restaurantInfo->customer_care_number }}</td>
                            </tr>


                            <tr>
                                <th>Hot Line Number Available</th>
                                <td>{{ $restaurantInfo->customer_care_service_duration }}</td>
                            </tr>




                            <tr>
                                <th>Restaurant Description</th>
                                <td>{{ $restaurantInfo->restaurant_description }}</td>
                            </tr>


                            <tr>
                                <th>Restaurant Logo</th>
                                <td><img style="height: 80px; width: 80px" src="{{ asset($restaurantInfo->restaurant_logo) }}" alt="Picture Not Found"></td>
                            </tr>

                            <tr>
                                <th>Restaurant Cover Photo</th>
                                <td><img style="height: 80px; width: 80px" src="{{ asset($restaurantInfo->restaurant_image)}}" alt="Picture Not Found"></td>
                            </tr>



                        </table>



                        <h2>Restaurant Open & Closed Info</h2>
                        <hr/>
                        <table class="table table-bordered table-striped">
                            <thead>

                            <tr>
                                <th>Saturday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->saturday)
                                        {{ $restaurantOpenTimeInfo->saturday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Sunday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->sunday)
                                        {{ $restaurantOpenTimeInfo->sunday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Monday</th>
                                <td>

                                    @isset($restaurantOpenTimeInfo->monday)
                                        {{ $restaurantOpenTimeInfo->monday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Tuesday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->tuesday)
                                        {{ $restaurantOpenTimeInfo->tuesday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Wednesday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->wednesday)
                                        {{ $restaurantOpenTimeInfo->wednesday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Thursday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->thursday)
                                        {{ $restaurantOpenTimeInfo->thursday  }}
                                    @endisset
                                </td>
                            </tr>

                            <tr>
                                <th>Friday</th>
                                <td>
                                    @isset($restaurantOpenTimeInfo->friday)
                                        {{ $restaurantOpenTimeInfo->friday  }}
                                    @endisset
                                </td>
                            </tr>


                        </table>




                        <h2>Restaurant Cuisines Information</h2>
                        <hr/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Cuisines Name</th>
                            </tr>
                            @php($i=1)
                            @foreach($restaurantCuisines as $restaurantCuisine)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $restaurantCuisine->cuisines_name }}</td>

                                </tr>
                            @endforeach
                        </table>



                        <h2>Delivery Time Information</h2>
                        <hr/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Delivery Time</th>
                            </tr>
                            @php($j=1)
                            @foreach($deliveryTimes as $deliveryTime)
                                <tr>
                                    <td>{{ $j++ }}</td>
                                    <td>{{ $deliveryTime->time }}</td>

                                </tr>
                            @endforeach
                        </table>



                        <h2>Restaurant Cuisines Information</h2>
                        <hr/><hr/>
                        <a href="{{ url('/restaurant-owner/'.$restaurantInfo->restaurant_id.'/verify-restaurant-owner') }}" class="btn btn-success" style="margin-left: 45%" title="Verify Restaurant">
                            <span class="glyphicon glyphicon-ok"></span> Verify Restaurant
                        </a>






                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection