<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            FAQ CONTENT
        </h1>

        @include('error')


    </section>

    <section class="content">

        <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Question</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-10">
                            <!-- Horizontal Form -->
                            <div class="box box-info">


                                <!-- form start -->
                                <form class="form-horizontal" method="POST" action="{{ url('/faq-content/new-faq-question-content') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="box-body">



                                        <div class="form-group">
                                            <label for="faq_title_id" class="col-sm-2 control-label">Faq Title Name</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="faq_title_id" id="faq_title_id" required>
                                                    <option value="">Select FAQ Title Name</option>
                                                    @foreach($publicationFaqTitleNames as $publicationFaqTitleName)
                                                        <option value="{{ $publicationFaqTitleName->id }}">{{ $publicationFaqTitleName->faq_title_name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="editor1" class="col-sm-2 control-label">Question</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control"  name="question"  required rows="10" placeholder="Maximum word 200" cols="80"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="editor2" class="col-sm-2 control-label">Answer</label>
                                            <div class="col-sm-10">
                                                <textarea id="editor1" name="answer"  required rows="10" cols="80"></textarea>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="publication_status" class="col-sm-2 control-label">Publication Status</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="publication_status" name="publication_status" required>
                                                    <option value="">Select Publication Status</option>
                                                    <option value="1">Published</option>
                                                    <option value="0">Unpublished</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <div class="col-sm-offset-2">
                                            <button type="submit" name="btn" class="btn btn-info">Save Faq Question Content</button>
                                        </div>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Video</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-10">
                                <!-- Horizontal Form -->
                                <div class="box box-info">


                                    <!-- form start -->
                                    <form class="form-horizontal" method="POST" action="{{ url('/faq-content/new-faq-video-content') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="box-body">



                                            <div class="form-group">
                                                <label for="faq_title_id" class="col-sm-2 control-label">Faq Title Name</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="faq_title_id" id="faq_title_id" required>
                                                        <option value="">Select FAQ Title Name</option>
                                                        @foreach($publicationFaqTitleNames as $publicationFaqTitleName)
                                                            <option value="{{ $publicationFaqTitleName->id }}">{{ $publicationFaqTitleName->faq_title_name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="editor3" class="col-sm-2 control-label">Question</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="question" required rows="10" placeholder="Maximum word 200" cols="20"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="editor4" class="col-sm-2 control-label">Video Link</label>
                                                <div class="col-sm-10">
                                                    <textarea id="editor2" name="video_link" required rows="10" cols="40"></textarea>
                                                </div>
                                            </div>




                                            <div class="form-group">
                                                <label for="publication_status" class="col-sm-2 control-label">Publication Status</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="publication_status" name="publication_status" required>
                                                        <option value="">Select Publication Status</option>
                                                        <option value="1">Published</option>
                                                        <option value="0">Unpublished</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputName3" class="col-sm-2 control-label">Video Image</label>
                                            <div class="col-sm-10">
                                                <input type="file" required name="image" accept="image/*"  id="inputName3">
                                            </div>
                                        </div>


                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                            <div class="col-sm-offset-2">
                                                <button type="submit" name="btn" class="btn btn-info">Save Faq Video Content</button>
                                            </div>
                                        </div>
                                        <!-- /.box-footer -->
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.box -->
            </div>
        </div>


    </section>
@endsection


@section('script')
    <!-- CK Editor -->
    <script src="{{asset('back-end/admin')}}/bower_components/ckeditor/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('back-end/admin')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor2')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })


    </script>
@endsection

