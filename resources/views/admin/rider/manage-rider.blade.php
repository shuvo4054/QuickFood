<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Rider
            <small>Manage Rider</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Rider Information Data Table</h3>

                        @if($message = Session::get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif

                        @if($message = Session::get('destroy'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL. Id</th>
                                <th>Rider Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>City</th>
                                <th>Living Address</th>
                                <th>Motor Or Bike</th>
                                <th>Driving License</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i = 1 @endphp
                            @foreach($riders as $rider)
                                <tr>
                                    <td>{{ $i++ }}</td>

                                    <td>{{ $rider->first_name }}{{ $rider->last_name }}</td>
                                    <td>{{ $rider->email }}</td>
                                    <td>{{ $rider->mobile_number }}</td>
                                    <td>{{ $rider->city }}</td>
                                    <td>{{ $rider->living_address }}</td>


                                    <td>
                                        @if($rider->motor == "yes")
                                            <span class="label label-success">Yes</span>
                                        @else
                                            <span class="label label-warning">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($rider->license == "yes")
                                            <span class="label label-success">Yes</span>
                                        @else
                                            <span class="label label-warning">No</span>
                                        @endif
                                    </td>

                                    <td>
                                        @if($rider->hire == 1)
                                            <a href="{{ url('/rider/not-hire-rider/'.$rider->id) }}" class="btn btn-success btn-xs" title="Hire">
                                                <span class="glyphicon glyphicon-arrow-up"></span>
                                            </a>
                                        @else
                                            <a href="{{ url('/rider/hire-rider/'.$rider->id) }}" class="btn btn-warning btn-xs" title="Not Hire">
                                                <span class="glyphicon glyphicon-arrow-down"></span>
                                            </a>
                                        @endif

                                        <a href="{{ url('/rider/delete-rider/'.$rider->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()

        })
    </script>
@endsection