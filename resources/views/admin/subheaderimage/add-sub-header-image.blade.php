<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Sub-Header-Image
            <small>Add Sub-Header-Image</small>
        </h1>

    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- Horizontal Form -->
                <div class="box box-info">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ url('add/new-sub-header-image') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('sub_header_image') ? 'has-error' : '' }}">
                                <label for="sub_header_image" class="col-sm-3 control-label">Sub-Header-Image</label>

                                <div class="col-sm-9">
                                    <input type="file" accept="image/*"   name="sub_header_image" required class="form-control" id="sub_header_image" >
                                    @if($errors->has('sub_header_image'))
                                        <span class="help-block">
                                        <strong class="text text-danger"> {{ $errors->first('sub_header_image') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-sm-offset-3">
                                <button type="submit" name="btn" class="btn btn-info">Save Sub-Header-Image</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>



@endsection