<!-- /.box -->
@extends('admin.master')

@section('body')
    <section class="content-header">
        <h1>
            Sub-Header-Images
            <small>Manage Sub-Header-Images</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Sub-Header-Image Data Table</h3>

                        @if($message = Session::get('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                                {{ $message }}
                            </div>
                        @endif

                        @if($message = Session::get('destroy'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $message }}
                            </div>
                        @endif
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sub-Header-Image</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)
                            @foreach($subHeaderImages as $subHeaderImage)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td><a href="{{ asset($subHeaderImage->sub_header_image) }}" class="thumbnail"><img src="{{ asset($subHeaderImage->sub_header_image)}}" style="height: 200px; width: 250px" alt="sub-header-image"></a></td>
                                    <td>
                                        <a href="{{ url('delete-sub-header-image/'.$subHeaderImage->id) }}" title="Delete Sub-Header-Image" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete this ?');">
                                            Delete
                                        </a>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{asset('/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection