@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/dropzone.css" rel="stylesheet">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Admin Profile</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <div id="position">
                <div class="container">
                    <ul>
                        <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                        </li>
                        <li>Admin Profile</li>
                    </ul>

                </div>
            </div><!-- Position -->


        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" class="active">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Profile Info</span></a>
                            </li>
                            <li><a href="#section-2" class="icon-menut-items"><span>Edit Profile</span></a>
                            </li>
                            <li><a href="#section-3" class="icon-settings"><span>Change Password</span></a>
                            </li>
                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <div class="row">

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon-right-hand"></i>
                                        <h3>First Name</h3>
                                        <p>
                                            {{ $restaurantAdmin->first_name }}
                                        </p>
                                    </div>

                                </div>

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon-right-hand"></i>
                                        <h3>Last Name</h3>
                                        <p>
                                            {{ $restaurantAdmin->last_name }}
                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_mail_alt"></i>
                                        <h3>Email</h3>
                                        <p>
                                            {{ $restaurantAdmin->email }}
                                        </p>
                                    </div>

                                </div>


                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon-phone"></i>
                                        <h3>Phone Number</h3>
                                        <p>
                                            {{ $restaurantAdmin->mobile_number }}
                                        </p>
                                    </div>

                                </div>



                            </div><!-- End row -->
                        </section>


                        <section id="section-2">
                            <div class="indent_title_in">
                                <i class="icon-edit"></i>
                                <h3>Edit Profile</h3>

                            </div>

                            <div class="wrapper_indent">


                                <form method="POST" action="{{ url('restaurant-admin/'.$restaurantAdmin->id) }}">
                                    {{ csrf_field() }}

                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="first_name">Your First Name</label>
                                        <input class="form-control" name="first_name"
                                               value="{{ $restaurantAdmin->first_name }}" id="first_name" required type="text">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="last_name">Your Last Name</label>
                                        <input class="form-control" name="last_name"
                                               value="{{ $restaurantAdmin->last_name }}" required id="last_name" type="text">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>




                                    <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                        <label for="mobile_number">Your Mobile Number</label>
                                        <input class="form-control" name="mobile_number"
                                               value="{{ $restaurantAdmin->mobile_number }}" required id="mobile_number"
                                               type="text">
                                        @if ($errors->has('mobile_number'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>





                                    <hr class="styled_2">
                                    <div class="wrapper_indent">
                                        <button type="submit" class="btn_1">Save now</button>
                                    </div><!-- End wrapper_indent -->
                                </form>
                            </div>

                        </section><!-- End section 1 -->


                        <section id="section-3">

                            <div class="row">

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_lock_alt"></i>
                                        <h3>Change your password</h3>

                                    </div>
                                    <div class="wrapper_indent">

                                        <form method="POST"
                                              action="{{ url('restaurant-admin/password/'.$restaurantAdmin->id) }}">
                                            {{ csrf_field() }}

                                            <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                                                <label for="old_password">Old password</label>
                                                <input class="form-control"  name="old_password" id="old_password"
                                                       type="password" required autofocus>
                                                @if ($errors->has('old_password'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password">Create a password</label>
                                                <input type="password" class="form-control" name="password" placeholder="Password"  id="password" required autofocus>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                @endif
                                            </div>


                                                <div class="form-group">
                                                    <label for="password_confirmation">Confirm password</label>
                                                    <input type="password" class="form-control" required name="password_confirmation" placeholder="Confirm password"  id="password_confirmation">
                                                </div>

                                            <button type="submit" class="btn_1 green">Update Password</button>
                                        </form>
                                    </div><!-- End wrapper_indent -->
                                </div>


                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_mail_alt"></i>
                                        <h3>Change your email</h3>

                                    </div>
                                    <div class="wrapper_indent">

                                        <form method="POST"
                                              action="{{ url('restaurant-admin/email/'.$restaurantAdmin->id) }}">
                                            {{ csrf_field() }}

                                            <div class="form-group {{ $errors->has('old_email') ? ' has-error' : '' }}">
                                                <label for="old_email">Old email</label>
                                                <input class="form-control" name="old_email" id="old_email"
                                                       type="email" required autofocus>
                                                @if ($errors->has('old_email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">New email</label>
                                                <input class="form-control" name="email" id="email"
                                                       type="email" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label for="email_confirmation">Confirm new email</label>
                                                <input class="form-control" name="email_confirmation"
                                                       id="email_confirmation"
                                                       type="email" required autofocus>
                                            </div>
                                            <button type="submit" class="btn_1 green">Update Email</button>
                                        </form>
                                    </div><!-- End wrapper_indent -->
                                </div>

                            </div><!-- End row -->




                        </section><!-- End section 3 -->

                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
    <script src="{{ asset('front-end') }}/js/dropzone.min.js"></script>
    <script>
        if ($('.dropzone').length > 0) {
            Dropzone.autoDiscover = false;
            $("#photos").dropzone({
                url: "upload",
                addRemoveLinks: true
            });

            $("#logo_picture").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });

            $(".menu-item-pic").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });
        }
    </script>
@endsection
