@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Order Details</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')


    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a></li>
                <li><a href="{{url('/manage-order')}}">Manage Order</a></li>
                <li>Order Details</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" >Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}" class="active">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">


                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-menu-items"><span>Order Information Data table</span></a></li>

                        </ul>
                    </nav>
                    <div class="content">


                        <section id="section-1">


                                    <div class="box">

                                        <!-- /.box-header -->
                                        @if($message = Session::get('message'))
                                            <h3 class="text-center text-success">{{ $message }}</h3>
                                        @endif
                                        <div class="box-body">
                                            <h2>Customer Info For This Order</h2>
                                            <hr/>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <td>{{ $customer->first_name.' '.$customer->last_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email Address</th>
                                                    <td>{{ $customer->email }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Mobile Number</th>
                                                    <td>{{ $customer->mobile_number }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Address</th>
                                                    <td>{{ $customer->living_address }},{{ $customer->city }}</td>
                                                </tr>
                                            </table>
                                            <h2>Shipping Info For This Order</h2>
                                            <hr/>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Shipping Name</th>
                                                    <td>{{ $shipping->full_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email Address</th>
                                                    <td>{{ $shipping->email }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Phone Number</th>
                                                    <td>{{ $shipping->mobile_number }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Address</th>
                                                    <td>{{ $shipping->living_address }}, {{ $shipping->city }}</td>
                                                </tr>

                                                <tr>
                                                    <th>Delivery schedule day & time</th>
                                                    <td>{{ $shipping->delivery_schedule_day }}, {{ $shipping->delivery_schedule_time }}</td>
                                                </tr>
                                            </table>
                                            <h2>Product Info For This Order</h2>
                                            <hr/>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SL No</th>
                                                    <th>Product Id</th>
                                                    <th>Product Image</th>
                                                    <th>Product Image</th>
                                                    <th>Extra Ingredient</th>
                                                    <th>Product Price</th>
                                                    <th>Product Quantity</th>
                                                    <th>Total Price</th>
                                                </tr>
                                                @php($i=1)
                                                @foreach($products as $product)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $product->product_id }}</td>
                                                        <td><img src="{{ asset($product->image) }}" alt="product image" style="height: 50px; width: 50px;"></td>
                                                        <td>{{ $product->product_name }}</td>
                                                        <td>{{ $product->ingredient }}</td>
                                                        <td>TK. {{ $product->product_price }}</td>
                                                        <td>{{ $product->product_quantity }}</td>
                                                        <td>৳ {{ $product->product_price * $product->product_quantity}}</td>

                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->



                        </section><!-- End section 2 -->





                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
