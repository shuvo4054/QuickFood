@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/dropzone.css" rel="stylesheet">


@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Update Order</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a></li>
                <li><a href="{{url('/manage-order')}}">Manage Order</a></li>
                <li>Update Order</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" >Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}" class="active">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">




                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Edit Order</span></a>
                            </li>


                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">
                            <form action="{{ url('/update-order') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $id }}"/>

                                <div class="indent_title_in">
                                    <i class="icon-edit-alt"></i>
                                    <h3>Update Order</h3>

                                </div>
                                <hr class="styled_2">

                                <div class="wrapper_indent">



                                    <div class="form-group">
                                        <label for="order_status">Order Status</label>
                                        <select class="form-control" name="order_status" id="order_status" required>
                                            {{-- <option value="" selected>Select your Option</option>--}}
                                            <option value="">Select Option</option>
                                            <option <?php if($order->order_status == "Pending"){echo "Selected";}else{echo "";}?> value="Pending">Pending</option>
                                            <option <?php if($order->order_status == "Cancel"){echo "Selected";}else{echo "";}?>  value="Cancel">Cancel</option>
                                            <option <?php if($order->order_status == "Successful"){echo "Selected";}else{echo "";}?>  value="Successful">Successful</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="payment_status">Payment Status</label>
                                        <select class="form-control" name="payment_status" id="payment_status" required>
                                            {{-- <option value="" selected>Select your Option</option>--}}
                                            <option  value="">Select Option</option>
                                            <option <?php if($payment->payment_status == "Pending"){echo "Selected";}else{echo "";}?>  value="Pending">Pending</option>
                                            <option <?php if($payment->payment_status == "Cancel"){echo "Selected";}else{echo "";}?>  value="Cancel">Cancel</option>
                                            <option <?php if($payment->payment_status == "Successful"){echo "Selected";}else{echo "";}?>  value="Successful">Successful</option>
                                        </select>
                                    </div>
                                </div>





                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Update Now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection





@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
    <script src="{{ asset('front-end') }}/js/dropzone.min.js"></script>


    <script>
        if ($('.dropzone').length > 0) {
            Dropzone.autoDiscover = false;
            $("#photos").dropzone({
                url: "upload",
                addRemoveLinks: true
            });

            $("#logo_picture").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });


        }
    </script>
@endsection
