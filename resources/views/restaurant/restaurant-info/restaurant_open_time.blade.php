@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">

    <link href="{{ asset('front-end') }}/css/date_time_picker.css" rel="stylesheet">
@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Restaurant Open & Closed Time Info</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')
    <div id="position">
        <div class="container">
            <div class="container">
                <ul>
                    <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                    </li>
                    <li>Open & Closed Time</li>
                </ul>

            </div>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}" >Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}" class="active">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                @if($message = Session::get('destroy'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif
                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-menut-items"><span>Open & Closed Time Info</span></a></li>
                            <li><a href="#section-2" class="icon-menut-items"><span>Edit Open & Close Time</span></a></li>

                        </ul>
                    </nav>
                    <div class="content">


                        <section id="section-1">
                            <div class="indent_title_in">
                                <i class="icon-restaurant"></i>
                                <h3>Restaurant Open & Close Day</h3>

                            </div>
                            <hr class="styled_2">


                            <div class="wrapper_indent">

                                    <div class="box_style_2">
                                        <h4 class="nomargin_top">Opening time <i class="icon_clock_alt pull-right"></i></h4>
                                        <ul class="opening_list">
                                            @isset($restaurantService)

                                            <li>Saturday<?php if ( $restaurantService->saturday == "closed") echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->saturday</span>";?></li>
                                            <li>Sunday<?php if ( $restaurantService->sunday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->sunday</span>";?></li>
                                            <li>Monday<?php if ( $restaurantService->monday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->monday</span>";?></li>
                                            <li>Tuesday<?php if ( $restaurantService->tuesday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->tuesday</span>";?></li>
                                            <li>Wednesday<?php if ( $restaurantService->wednesday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->wednesday</span>";?></li>
                                            <li>Thursday<?php if ( $restaurantService->thursday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->thursday</span>";?></li>
                                            <li>Friday<?php if ( $restaurantService->friday == "closed" ) echo "<span class='label label-danger'>Closed</span>"; else echo "<span>$restaurantService->friday</span>";?></li>

                                            @endisset
                                        </ul>
                                    </div>
                            </div>




                        </section><!-- End section 2 -->


                        <section id="section-2">
                            <form method="POST" action="{{ route('save.restaurant.OpenTime') }}">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon-edit-alt"></i>
                                    <h3>Edit Restaurant Open & Closed Time</h3>

                                </div>

                                <hr class="styled_2">


                                <div class="wrapper_indent">

                                    <div class="form-group {{ $errors->has('saturday') ? ' has-error' : '' }}">
                                        <label for="Saturday">Saturday</label>
                                        <input type="text" id="Saturday" name="saturday" class="form-control" required value="<?php if ( $restaurantService->saturday == "0" ) echo ""; else echo "$restaurantService->saturday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('saturday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('saturday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('sunday') ? ' has-error' : '' }}">
                                        <label for="sunday">Sunday</label>
                                        <input type="text" id="sunday" name="sunday" class="form-control" required value="<?php if ( $restaurantService->sunday == "0" ) echo ""; else echo "$restaurantService->sunday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('sunday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('sunday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('monday') ? ' has-error' : '' }}">
                                        <label for="monday">Monday</label>
                                        <input type="text" id="monday" name="monday" class="form-control" required value="<?php if ( $restaurantService->monday == "0" ) echo ""; else echo "$restaurantService->monday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('monday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('monday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('tuesday') ? ' has-error' : '' }}">
                                        <label for="tuesday">Tuesday</label>
                                        <input type="text" id="tuesday" name="tuesday" class="form-control" required value="<?php if ( $restaurantService->tuesday == "0" ) echo ""; else echo "$restaurantService->tuesday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('restaurant_logo'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('tuesday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('wednesday') ? ' has-error' : '' }}">
                                        <label for="wednesday">Wednesday</label>
                                        <input type="text" id="wednesday" name="wednesday" class="form-control" required value="<?php if ( $restaurantService->wednesday == "0" ) echo ""; else echo "$restaurantService->wednesday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('wednesday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('wednesday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('thursday') ? ' has-error' : '' }}">
                                        <label for="thursday">Thursday</label>
                                        <input type="text" id="thursday" name="thursday" class="form-control" required value="<?php if ( $restaurantService->thursday == "0" ) echo ""; else echo "$restaurantService->thursday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('thursday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('thursday') }}</strong>
                                    </span>
                                                @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('friday') ? ' has-error' : '' }}">
                                        <label for="friday">Friday</label>
                                        <input type="text" id="friday" name="friday" class="form-control" required value="<?php if ( $restaurantService->friday == "0" ) echo ""; else echo "$restaurantService->friday";?>" placeholder="12.00am-11.00pm or closed">
                                        @if ($errors->has('friday'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('friday') }}</strong>
                                    </span>
                                                @endif
                                    </div>




                                </div>







                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection



@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>


    <!-- Date and time pickers -->
    <script src="{{ asset('front-end') }}/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('front-end') }}/js/bootstrap-timepicker.js"></script>
    <script>
        $('input.date-pick').datepicker('setDate', 'today');
        $('input.time-pick').timepicker({
            minuteStep: 15,
            showInpunts: false
        })
    </script>

@endsection
