<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from zebratheme.com/html/fooadmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Feb 2018 04:17:22 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Foodmin : Admin Kit</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/weather-icons.css" rel="stylesheet" />
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/mmc-chat.css" rel="stylesheet" />
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/sidebar.css" rel="stylesheet">
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/lib/unix.css" rel="stylesheet">
    <link href="{{ asset('back-end/restaurant-dashboard') }}/assets/css/style.css" rel="stylesheet">

    @yield('link')


</head>

<body>

@include('restaurant.includes.sidebar')



@include('restaurant.includes.header')



<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            @yield('body')

        </div><!-- /# container-fluid -->
    </div><!-- /# main -->
</div><!-- /# content wrap -->




<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/jquery.min.js"></script><!-- jquery vendor -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/jquery.nanoscroller.min.js"></script><!-- nano scroller -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/sidebar.js"></script><!-- sidebar -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/bootstrap.min.js"></script><!-- bootstrap -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/mmc-common.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/mmc-chat.js"></script>
<!--  Chart js -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/chart-js/Chart.bundle.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/chart-js/chartjs-init.js"></script>
<!-- // Chart js -->

<!--  Datamap -->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/datamap/d3.min.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/datamap/topojson.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/datamap/datamaps.world.min.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/datamap/datamap-init.js"></script>
<!-- // Datamap -->-->
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/weather/jquery.simpleWeather.min.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/weather/weather-init.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/lib/owl-carousel/owl.carousel-init.js"></script>
<script src="{{ asset('back-end/restaurant-dashboard') }}/assets/js/scripts.js"></script><!-- scripit init-->



@yield('script')

</body>
</html>
