@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('back-end/admin') }}/bower_components/jvectormap/jquery-jvectormap.css">
@endsection
@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Food Item Ingredients</h1>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a></li>
                <li><a href="{{ url('/menu-item/manage-menu-item') }}">Menu Item</a></li>

                <li>Food Item Ingredients</li>
            </ul>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}"  class="active">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">

                @if($message = Session::get('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif

                @if($message = Session::get('destroy'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ $message }}
                    </div>
                @endif
                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-menut-items"><span>Manage Item Ingredient</span></a></li>
                            <li><a href="#section-2" class="icon-profile"><span>Add Item Ingredient</span></a></li>


                        </ul>
                    </nav>
                    <div class="content">


                        <section id="section-1">
                            <div class="indent_title_in">
                                <i class="icon-table"></i>
                                <h3>Food Item Ingredient Data Table</h3>
                            </div>
                            <hr class="styled_2">

                            <div class="box-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped ">
                                    <thead>
                                    <tr>
                                        <th>SL No.</th>
                                        <th>Ingredient Name</th>
                                        <th>Ingredient Price</th>
                                        <th>Action</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($i=1)
                                    @foreach($ingredients as $ingredient)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $ingredient->ingredient_name }}</td>
                                            <td>{{ $ingredient->ingredient_price }}</td>



                                            <td>

                                                <a href="{{ url('/ingredient/delete-ingredient/'.$ingredient->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete This ?');">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>

                                            </td>



                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>





                        </section><!-- End section 2 -->


                        <section id="section-2">
                            <form method="POST" action="{{ route('save.item-ingredient') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon-list-add"></i>
                                    <h3>Add Food Item Ingredient</h3>

                                </div>
                                <hr class="styled_2">

                                <div class="wrapper_indent">



                                    <div class="form-group {{ $errors->has('ingredient_name') ? ' has-error' : '' }}">
                                        <label for="ingredient_name">Ingredient Name</label>
                                        <input class="form-control" name="ingredient_name" id="ingredient_name" type="text" required placeholder="Enter Ingredient Name..Ex: Extra Vegetable">
                                        <input class="form-control" name="restaurant_menu_items_id" value="{{$id}}"  type="hidden">
                                        @if ($errors->has('ingredient_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('ingredient_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>




                                    <div class="form-group {{ $errors->has('ingredient_price') ? ' has-error' : '' }}">
                                        <label for="ingredient_price">Ingredient Price</label>
                                        <input class="form-control" name="ingredient_price" id="ingredient_price" type="text" required placeholder="৳ 50">
                                        @if ($errors->has('ingredient_price'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('ingredient_price') }}</strong>
                                    </span>
                                        @endif
                                    </div>



                                </div>



                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <!-- DataTables -->
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back-end/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{asset('back-end/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>







    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
