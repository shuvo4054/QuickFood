@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset('front-end')}}/img/sub_header_2.jpg" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Admin section</h1>
                <p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>
                <p></p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a>
                </li>
                <li><a href="#0">Category</a>
                </li>
                <li>Page active</li>
            </ul>
            <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}"  class="active">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">


                <div id="tabs" class="tabs">
                    <nav>
                        <ul>

                            <li><a href="#section-1" class="icon-profile"><span>Edit Menu</span></a></li>


                        </ul>
                    </nav>
                    <div class="content">





                        <section id="section-1">
                            <form method="POST" action="{{ url('/menu-item/update-menu-item').'/'.$menuItem->id }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon_house_alt"></i>
                                    <h3>General restaurant description</h3>
                                    <p>Partem diceret praesent mel et, vis facilis alienum antiopam ea, vim in sumo diam
                                        sonet. Illud ignota cum te, decore elaboraret nec ea. Quo ei graeci repudiare
                                        definitionem. Vim et malorum ornatus assentior, exerci elaboraret eum ut, diam
                                        meliore no mel.</p>
                                </div>

                                <div class="wrapper_indent">


                                    <div class="form-group">
                                        <label for="publication_status">Food Menu</label>
                                        <select class="form-control" id="publication_status" name="restaurant_menu_id">
                                            <option>Select Food Menu</option>

                                            @foreach($menus as $menu)
                                                <option <?php if ($menuItem->restaurant_menu_id == $menu->id) { echo "selected";} else{ echo "";}  ?> value="{{ $menu->id }}">{{ $menu->menu_name }}</option>
                                            @endforeach

                                        </select>
                                    </div>



                                    <div class="form-group">
                                        <label for="item_name">Item Name</label>
                                        <input class="form-control" name="item_name" value="{{ $menuItem->item_name }}" id="item_name" type="text">
                                    </div>

                                    <div class="form-group">
                                        <label for="item_description">Item Description</label>
                                        <textarea class="form-control" name="item_description" id="item_description" >{{ $menuItem->item_description }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <img src="{{ asset($menuItem->item_image)}}" style="height: 100px; width: 110px" title="Item Image" alt="restaurant-menu-item-image">
                                    </div>



                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>Restaurant Item Image</label>
                                            <input type="file" accept="image/*"   name="item_image"  class="form-control">
                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <label for="item_price">Item Price</label>
                                        <input class="form-control" name="item_price"  value="{{ $menuItem->item_price }}"  id="item_price" type="text">
                                    </div>


                                    <div class="form-group">
                                        <label for="publication_status">Publication Status</label>
                                        <select class="form-control" id="publication_status" name="publication_status">
                                            <option>Select Publication Status</option>
                                            <option <?php if ($menuItem->publication_status == '1') { echo "selected";} else{ echo "";}  ?> value="1">Published</option>
                                            <option <?php if ($menuItem->publication_status == '0') { echo "selected";} else{ echo "";}  ?> value="0">Unpublished</option>
                                        </select>
                                    </div>

                                </div>




                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
