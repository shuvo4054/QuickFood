@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <?php
    $coverPicture = new \App\Http\Controllers\ManageRestaurant\RestaurantAdminController();
    $coverPicture = $coverPicture->coverPicture();
    ?>
    <section class="parallax-window" id="short" data-parallax="scroll"
             data-image-src="{{asset($coverPicture->restaurant_image)}}" data-natural-width="1400"
             data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Edit Restaurant Menu Item</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')


    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{route('restaurant.dashboard')}}">Dashboard</a>
                </li>
                <li>Edit Restaurant Menu Item</li>
            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}">Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}"  class="active">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->

            <div class="col-md-9">


                <div id="tabs" class="tabs">
                    <nav>
                        <ul>

                            <li><a href="#section-1" class="icon-profile"><span>Edit Menu</span></a></li>


                        </ul>
                    </nav>
                    <div class="content">





                        <section id="section-1">
                            <form method="POST" action="{{ url('/menu-item/update-menu-item').'/'.$menuItem->id }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="indent_title_in">
                                    <i class="icon-edit-alt"></i>
                                    <h3>Edit Restaurant Menu Item</h3>

                                </div>
                                <hr class="styled_2">

                                <div class="wrapper_indent">

                                    <div class="form-group {{ $errors->has('restaurant_menu_id') ? ' has-error' : '' }}">
                                        <label for="publication_status">Food Menu</label>
                                        <select class="form-control" id="publication_status" name="restaurant_menu_id" required>
                                            <option value="">Select Food Menu</option>
                                            @foreach($menus as $menu)
                                                <option <?php if ($menuItem->restaurant_menu_id == $menu->id) { echo "selected";} else{ echo "";}  ?> value="{{ $menu->id }}">{{ $menu->menu_name }}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('restaurant_menu_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('restaurant_menu_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>



                                    <div class="form-group {{ $errors->has('item_name') ? ' has-error' : '' }}">
                                        <label for="item_name">Item Name</label>
                                        <input class="form-control" name="item_name" value="{{ $menuItem->item_name }}" id="item_name" type="text" required placeholder="Enter Food Menu Item...Ex: Cheesecake ">
                                        @if ($errors->has('item_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('item_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('cuisines_name') ? ' has-error' : '' }}">
                                        <label for="item_description">Item Description</label>
                                        <textarea class="form-control" name="item_description" id="item_description" required placeholder="Enter text.....">{{ $menuItem->item_description }}</textarea>
                                        @if ($errors->has('menu_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('menu_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <img src="{{ asset($menuItem->item_image)}}" style="height: 100px; width: 110px" title="Item Image" alt="restaurant-menu-item-image">

                                    </div>



                                        <div class="form-group {{ $errors->has('item_image') ? ' has-error' : '' }}">
                                            <label>Restaurant Item Image</label>
                                            <input type="file" accept="image/*"   name="item_image" required  class="form-control">
                                            @if ($errors->has('item_image'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('item_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>




                                    <div class="form-group {{ $errors->has('item_price') ? ' has-error' : '' }}">
                                        <label for="item_price">Item Price</label>
                                        <input class="form-control" name="item_price"  value="{{ $menuItem->item_price }}"  id="item_price" type="text" required placeholder="৳ 250">
                                        @if ($errors->has('item_price'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('item_price') }}</strong>
                                    </span>
                                        @endif
                                    </div>


                                    <div class="form-group {{ $errors->has('publication_status') ? ' has-error' : '' }}">
                                        <label for="publication_status">Publication Status</label>
                                        <select class="form-control" id="publication_status" name="publication_status" required>
                                            <option value="">Select Publication Status</option>
                                            <option <?php if ($menuItem->publication_status == '1') { echo "selected";} else{ echo "";}  ?> value="1">Published</option>
                                            <option <?php if ($menuItem->publication_status == '0') { echo "selected";} else{ echo "";}  ?> value="0">Unpublished</option>
                                        </select>
                                        @if ($errors->has('publication_status'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('publication_status') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>




                                <hr class="styled_2">




                                <div class="wrapper_indent">

                                    <button type="submit" name="btn" class="btn_1">Save now</button>
                                </div><!-- End wrapper_indent -->
                            </form>

                        </section><!-- End section 1 -->



                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..."/>
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')
    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
@endsection
