@extends('front.master')

@section('link')

    <!-- SPECIFIC CSS -->
    <link href="{{ asset('front-end') }}/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/admin.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="{{ asset('front-end') }}/css/dropzone.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/back-end/admin') }}/dist/css/AdminLTE.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/back-end/admin') }}/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('/back-end/admin') }}/bower_components/Ionicons/css/ionicons.min.css">

@endsection

@section('subheader')

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ asset($coverPicture->restaurant_image) }}" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Dashboard</h1>

            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->
@endsection



@section('body')

    <div id="position">
        <div class="container">
            <ul>
                <li>Dashboard</li>

            </ul>

        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">

            <div class="col-md-3">
                <p><a href="{{ route('restaurant.dashboard') }}" class="btn_side">Dashboard</a></p>
                <div class="box_style_1">
                    <ul id="cat_nav">
                        <li><a href="{{ route('restaurant.dashboard') }}" class="active">Dashboard </a></li>
                        <li><a href="{{ route('restaurant-admin.profile') }}" >Admin Profile </a></li>
                        <li><a href="{{ route('show.restaurant-info') }}" >Restaurant Info </a></li>
                        <li><a href="{{ route('restaurant.sub-images') }}" >Restaurant Slider Images </a></li>
                        <li><a href="{{ route('restaurant.cuisines') }}">Restaurant Cuisines</a></li>
                        <li><a href="{{ route('restaurant.OpenTime') }}">Restaurant Service Day </a></li>
                        <li><a href="{{ route('manage.delivery.time') }}">Restaurant Delivery Time </a></li>
                        <li><a href="{{ route('restaurant.menu') }}">Restaurant Menu </a></li>
                        <li><a href="{{ route('menu.item') }}">Menu Item </a></li>
                        <li><a href="{{ route('restaurant.order') }}">Manage Order </a></li>
                    </ul>
                </div><!-- End box_style_1 -->
            </div><!-- End col-md-3 -->



            <div class="col-md-9">

                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            <li><a href="#section-1" class="icon-profile"><span>Order info</span></a>
                            </li>

                           {{-- <li><a href="#section-2" class="icon-menut-items"><span>Menu</span></a>
                            </li>
                            <li><a href="#section-3" class="icon-settings"><span>Settings</span></a>
                            </li>--}}
                        </ul>
                    </nav>
                    <div class="content">

                        <section id="section-1">

                            <div class="row">
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-blue">
                                        <div class="inner">
                                            <h3>{{ $newOrder }}</h3>

                                            <p>New Orders</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                        <a href="{{ route('restaurant.order') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>



                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-gray">
                                        <div class="inner">
                                            <h3>{{ $todayOrder }}</h3>

                                            <p>Today Orders</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                        <a href="{{ route('restaurant.order') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>



                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3>{{ $successfulOrder }}</h3>

                                            <p>Successfull Orders</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                        <a href="{{ route('restaurant.order') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>



                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                        <div class="inner">
                                            <h3>{{ $cancelOrder}}</h3>

                                            <p>Cancel Orders</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                        <a href="{{ route('restaurant.order') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>





                            {{-- <!-- ./col -->
                             <div class="col-lg-4 col-xs-6">
                                 <!-- small box -->
                                 <div class="small-box bg-green">
                                     <div class="inner">
                                         <h3>53<sup style="font-size: 20px">%</sup></h3>

                                         <p>Bounce Rate</p>
                                     </div>
                                     <div class="icon">
                                         <i class="ion ion-stats-bars"></i>
                                     </div>
                                     <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                 </div>
                             </div>

                                <!-- ./col -->
                                <div class="col-lg-4 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>44</h3>

                                            <p>User Registrations</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-person-add"></i>
                                        </div>
                                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>--}}
                                <!-- ./col -->


                            </div>


                        </section><!-- End section 1 -->

                        <section id="section-2">
                            <div class="indent_title_in">
                                <i class="icon_document_alt"></i>
                                <h3>Edit menu list</h3>
                                <p>Partem diceret praesent mel et, vis facilis alienum antiopam ea, vim in sumo diam sonet. Illud ignota cum te, decore elaboraret nec ea. Quo ei graeci repudiare definitionem. Vim et malorum ornatus assentior, exerci elaboraret eum ut, diam meliore no mel.</p>
                            </div>

                            <div class="wrapper_indent">
                                <div class="form-group">
                                    <label>Menu Category</label>
                                    <input type="text" name="menu_category" class="form-control" placeholder="EX. Starters">
                                </div>

                                <div class="menu-item-section clearfix">
                                    <h4>Menu item #1</h4>
                                    <div><a href="#0"><i class="icon_plus_alt"></i></a><a href="#0"><i class="icon_minus_alt"></i></a>
                                    </div>
                                </div>

                                <div class="strip_menu_items">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="menu-item-pic dropzone">
                                                <input name="file" type="file">
                                                <div class="dz-default dz-message"><span>Click or Drop<br>Images Here</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <input type="text"  name="menu_item_title" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input type="text" name="menu_item_title_price" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Short description</label>
                                                <input type="text" name="menu_item_description" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Item options</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped edit-options">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $3.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Medium">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_1" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_1" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Large">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_2" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_2" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->

                                            <div class="form-group">
                                                <label>Item ingredients</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped notifications">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $2.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra tomato">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_3" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_3" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra Pepper">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_4" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_4" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->
                                        </div>
                                    </div><!-- End row -->
                                </div><!-- End strip_menu_items -->



                                <div class="menu-item-section clearfix">
                                    <h4>Menu item #2</h4>
                                    <div><a href="#0"><i class="icon_plus_alt"></i></a><a href="#0"><i class="icon_minus_alt"></i></a>
                                    </div>
                                </div>

                                <div class="strip_menu_items">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="menu-item-pic dropzone">
                                                <input name="file" type="file">
                                                <div class="dz-default dz-message"><span>Click or Drop<br>Images Here</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <input type="text" name="menu_item_title" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input type="text" name="menu_item_title_price" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Short description</label>
                                                <input type="text" name="menu_item_description" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Item options</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped notifications">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $3.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Medium">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_5" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_5" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Large">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_7" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_7" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->

                                            <div class="form-group">
                                                <label>Item ingredients</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped notifications">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $2.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra tomato">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_8" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_8" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra Pepper">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_9" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_9" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->
                                        </div>
                                    </div><!-- End row -->
                                </div><!-- End strip_menu_items -->
                            </div><!-- End wrapper_indent -->

                            <hr class="styled_2">

                            <div class="wrapper_indent">
                                <div class="form-group">
                                    <label>Menu Category</label>
                                    <input type="text" name="menu_category" class="form-control" placeholder="EX. Main courses">
                                </div>

                                <div class="menu-item-section clearfix">
                                    <h4>Menu item #1</h4>
                                    <div><a href="#0"><i class="icon_plus_alt"></i></a><a href="#0"><i class="icon_minus_alt"></i></a>
                                    </div>
                                </div>

                                <div class="strip_menu_items">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="menu-item-pic dropzone">
                                                <input name="file" type="file">
                                                <div class="dz-default dz-message"><span>Click or Drop<br>Images Here</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <input type="text" name="menu_item_title" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input type="text" name="menu_item_title_price" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Short description</label>
                                                <input type="text" name="menu_item_description" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Item options</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped notifications">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $3.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Medium">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_10" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_10" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Large">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_11" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_11" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->

                                            <div class="form-group">
                                                <label>Item ingredients</label>
                                                <div class="table-responsive">
                                                    <table class="table table-striped notifications">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $2.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra tomato">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_12" checked class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_12" class="icheck" value="radio">Radio</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:20%">
                                                                <input type="text" class="form-control" placeholder="+ $5.50">
                                                            </td>
                                                            <td style="width:50%">
                                                                <input type="text" class="form-control" placeholder="Ex. Extra Pepper">
                                                            </td>
                                                            <td style="width:30%">
                                                                <label>
                                                                    <input type="radio" name="option_item_settings_13" class="icheck" value="checkbox">Checkbox</label>
                                                                <label class="margin_left">
                                                                    <input type="radio" name="option_item_settings_13" class="icheck" value="radio" checked>Radio</label>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div><!-- End form-group -->
                                        </div>
                                    </div><!-- End row -->
                                </div><!-- End strip_menu_items -->
                            </div><!-- End wrapper_indent -->

                            <hr class="styled_2">

                            <div class="wrapper_indent">
                                <div class="add_more_cat"><a href="#0" class="btn_1">Save now</a> <a href="#0" class="btn_1">Add menu category</a> </div>
                            </div><!-- End wrapper_indent -->

                        </section><!-- End section 2 -->

                        <section id="section-3">

                            <div class="row">

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_lock_alt"></i>
                                        <h3>Change your password</h3>
                                        <p>
                                            Mussum ipsum cacilds, vidis litro abertis.
                                        </p>
                                    </div>
                                    <div class="wrapper_indent">
                                        <div class="form-group">
                                            <label>Old password</label>
                                            <input class="form-control" name="old_password" id="old_password" type="password">
                                        </div>
                                        <div class="form-group">
                                            <label>New password</label>
                                            <input class="form-control" name="new_password" id="new_password" type="password">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm new password</label>
                                            <input class="form-control" name="confirm_new_password" id="confirm_new_password" type="password">
                                        </div>
                                        <button type="submit" class="btn_1 green">Update Password</button>
                                    </div><!-- End wrapper_indent -->
                                </div>

                                <div class="col-md-6 col-sm-6 add_bottom_15">
                                    <div class="indent_title_in">
                                        <i class="icon_mail_alt"></i>
                                        <h3>Change your email</h3>
                                        <p>
                                            Mussum ipsum cacilds, vidis litro abertis.
                                        </p>
                                    </div>
                                    <div class="wrapper_indent">
                                        <div class="form-group">
                                            <label>Old email</label>
                                            <input class="form-control" name="old_email" id="old_email" type="email">
                                        </div>
                                        <div class="form-group">
                                            <label>New email</label>
                                            <input class="form-control" name="new_email" id="new_email" type="email">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm new email</label>
                                            <input class="form-control" name="confirm_new_email" id="confirm_new_email" type="email">
                                        </div>
                                        <button type="submit" class="btn_1 green">Update Email</button>
                                    </div><!-- End wrapper_indent -->
                                </div>

                            </div><!-- End row -->

                            <hr class="styled_2">

                            <div class="indent_title_in">
                                <i class="icon_shield"></i>
                                <h3>Notification settings</h3>
                                <p>
                                    Mussum ipsum cacilds, vidis litro abertis.
                                </p>
                            </div>
                            <div class="row">

                                <div class="col-md-6 col-sm-6">
                                    <div class="wrapper_indent">
                                        <table class="table table-striped notifications">
                                            <tbody>
                                            <tr>
                                                <td style="width:5%">
                                                    <i class="icon_pencil-edit_alt"></i>
                                                </td>
                                                <td style="width:65%">
                                                    New orders
                                                </td>
                                                <td style="width:35%">
                                                    <label>
                                                        <input type="checkbox" name="option_1_settings" checked class="icheck" value="yes">Yes</label>
                                                    <label class="margin_left">
                                                        <input type="checkbox" name="option_1_settings" class="icheck" value="no">No</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="icon_pencil-edit_alt"></i>
                                                </td>
                                                <td>
                                                    Modified orders
                                                </td>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" name="option_2_settings" checked class="icheck" value="yes">Yes</label>
                                                    <label class="margin_left">
                                                        <input type="checkbox" name="option_2_settings" class="icheck" value="no">No</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="icon_pencil-edit_alt"></i>
                                                </td>
                                                <td>
                                                    New user registration
                                                </td>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" name="option_3_settings" checked class="icheck" value="yes">Yes</label>
                                                    <label class="margin_left">
                                                        <input type="checkbox" name="option_3_settings" class="icheck" value="no">No</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class="icon_pencil-edit_alt"></i>
                                                </td>
                                                <td>
                                                    New comments
                                                </td>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" name="option_4_settings" checked class="icheck" value="yes">Yes</label>
                                                    <label class="margin_left">
                                                        <input type="checkbox" name="option_4_settings" class="icheck" value="no">No</label>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button type="submit" class="btn_1 green">Update notifications settings</button>
                                    </div>

                                </div><!-- End row -->
                            </div><!-- End wrapper_indent -->

                        </section><!-- End section 3 -->

                    </div><!-- End content -->
                </div>

            </div><!-- End col-md-6 -->


        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

@endsection




@section('search-menu')

    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
@endsection


@section('script')

    <!-- Specific scripts -->
    <script src="{{ asset('front-end') }}/js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));
    </script>

    <script src="{{ asset('front-end') }}/js/bootstrap3-wysihtml5.min.js"></script>
    <script type="text/javascript">
        $('.wysihtml5').wysihtml5({});
    </script>
    <script src="{{ asset('front-end') }}/js/dropzone.min.js"></script>
    <script>
        if ($('.dropzone').length > 0) {
            Dropzone.autoDiscover = false;
            $("#photos").dropzone({
                url: "upload",
                addRemoveLinks: true
            });

            $("#logo_picture").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });

            $(".menu-item-pic").dropzone({
                url: "upload",
                maxFiles: 1,
                addRemoveLinks: true
            });
        }
    </script>
@endsection
