<?php

namespace App\Http\Controllers\ManageAdmin;

use App\FaqContent;
use App\FaqTitel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class FaqContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    // For Faq title start
    public function addFaqContentInfo()
    {
        $publicationFaqTitleNames = FaqTitel::where('publication_status','=',1)->get();
        return view('admin.faq.faq-content.add-faq-content',compact('publicationFaqTitleNames'));
    }


    public function saveFaqQuestionContentInfo(Request $request)
    {
        $this->validate($request,[
            'faq_title_id' => 'required',
            'question' => 'required|string',
            'answer' => 'required',
            'publication_status' => 'required'

        ]);

        $faqContent = new FaqContent();
        $faqContent->faq_title_id = $request->faq_title_id;
        $faqContent->question = $request->question;
        $faqContent->answer = $request->answer;
        $faqContent->publication_status = $request->publication_status;
        $faqContent->save();





        return redirect('/faq-content/manage-faq-content')->with('message','Faq Question Content info add successfully');
    }

    public function saveFaqVideoContentInfo(Request $request)
    {


        $this->validate($request, [
            'faq_title_id' => 'required',
            'question' => 'required',
            'video_link' => 'required',

            'publication_status' => 'required',


        ]);




        $videoTitleImage = $request->file('image');
        $uniqueName = substr(md5(time()),'0','10');
        $uniqueImageName = $uniqueName.'.'.$videoTitleImage->getClientOriginalExtension();
        $directory = 'images/admin/faq-video-title-image';
        $imageUrl = $directory.$uniqueImageName;
        Image::make($videoTitleImage)->save($imageUrl);





        $faqContent = new FaqContent();
        $faqContent->faq_title_id = $request->faq_title_id;
        $faqContent->question = $request->question;
        $faqContent->video_link = $request->video_link;
        $faqContent->image = $imageUrl;
        $faqContent->publication_status = $request->publication_status;
        $faqContent->save();

        return redirect('/faq-content/manage-faq-content')->with('message','Faq Video Content info add successfully');
    }


    public function manageFaqContentInfo(){

        $faqContents = FaqContent::orderBy('id', 'desc')->get();

        return view('admin.faq.faq-content.manage-faq-content',compact('faqContents'));
    }

    public function unpublishedFaqContentInfo($data){
        $faqContentById = FaqContent::find($data);
        $faqContentById->publication_status = 0;
        $faqContentById->save();

        return redirect('/faq-content/manage-faq-content')->with('message','Faq Content info unpublished');


    }

    public function publishedFaqContentInfo($data){
        $faqContentById = FaqContent::find($data);
        $faqContentById->publication_status = 1;
        $faqContentById->save();

        return redirect('/faq-content/manage-faq-content')->with('message','Faq Content info published');


    }


    public function editFaqContentInfo($id)
    {
        $faqContent = FaqContent::find($id);
        return view('admin.faq.faq-content.edit-faq-content',compact('faqContent'));
    }


    public function updateFaqContentInfo(Request $request, $id){
        $faqContentById = FaqContent::find($id);
        $faqContentById->faq_content_name = $request->faq_content_name;
        $faqContentById->publication_status = $request->publication_status;
        $faqContentById->save();
        return redirect('/faq-content/manage-faq-content')->with('message','Faq Content Update Successfully');
    }

    public function deleteFaqContentInfo($id)
    {
        $faqContentById = FaqContent::find($id);

        if ($faqContentById->image) {

            @unlink($faqContentById->image);
        }


        $faqContentById->delete();
        return redirect('/faq-content/manage-faq-content')->with('destroy','Faq Content Delete Successfully');
    }
    // Faq Content End

}
