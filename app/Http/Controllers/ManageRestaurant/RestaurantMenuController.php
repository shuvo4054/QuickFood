<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\RestaurantMenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RestaurantMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }

    public function manageRestaurantMenuInfo()
    {
         $menus =  RestaurantMenu::where('restaurant_unique_id',Auth::user()->unique_id)->get();
        return view('restaurant.restaurant-food-menu.menu.manage-menu',compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveRestaurantMenuInfo(Request $request)
    {
        $this->validate($request, [
        'menu_name' => 'required|string',
        'menu_description' => 'required|string|max:500',
        'publication_status' => 'required|string|max:300',

    ]);


        $menu = new RestaurantMenu();
        $menu->restaurant_unique_id = Auth::user()->unique_id;
        $menu->menu_name = $request->menu_name;
        $menu->menu_description = $request->menu_description;
        $menu->publication_status = $request->publication_status;
        $menu->save();

        return redirect()->back()->with('message', 'Restaurant Food Menu Info Inserted Successfully');
    }

    public function unpublishedRestaurantMenuInfo($data){
        $menuById = RestaurantMenu::find($data);
        $menuById->publication_status = 0;
        $menuById->save();

        return redirect()->back()->with('destroy','Restaurant Food Menu unpublished');


    }

    public function publishedRestaurantMenuInfo($data){
        $menuById = RestaurantMenu::find($data);
        $menuById->publication_status = 1;
        $menuById->save();

        return redirect()->back()->with('message','Restaurant Food Menu published');


    }


    public function editRestaurantMenuInfo($id)
    {
        $menu = RestaurantMenu::find($id);
        return view('restaurant.restaurant-food-menu.menu.edit-menu',compact('menu'));
    }



    public function updateRestaurantMenuInfo(Request $request, $id){

        $this->validate($request, [
            'menu_name' => 'required|string',
            'menu_description' => 'required|string|max:500',
            'publication_status' => 'required|string|max:300',

        ]);

        $menu = RestaurantMenu::find($id);
        $menu->menu_name = $request->menu_name;
        $menu->menu_description = $request->menu_description;
        $menu->publication_status = $request->publication_status;
        $menu->save();
        return redirect()->action('ManageRestaurant\RestaurantMenuController@manageRestaurantMenuInfo')->with('message', 'Restaurant Food Menu Info Updated Successfully');
    }



    public function deleteRestaurantMenuInfo($id){
        $menu = RestaurantMenu::find($id);
        $menu->delete();
        return redirect()->back()->with('destroy','Restaurant Food Menu Deleted Successfully');

    }



}
