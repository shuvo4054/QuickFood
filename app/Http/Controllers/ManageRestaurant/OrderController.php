<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\Order;
use App\OrderDetail;
use App\Payment;
use App\RestaurantMenuItem;
use App\Shipping;
use App\User;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }


    public function manageOrderInfo()
    {


         $todayOrders = DB::table('orders')
            ->join('users', 'orders.customer_id', '=', 'users.id')
            ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
            ->join('payments', 'orders.id', '=', 'payments.order_id')
            ->select('orders.*', 'shippings.delivery_schedule_day as delivery_schedule_day', 'users.first_name as first_name', 'users.last_name as last_name', 'payments.payment_type as payment_type', 'payments.payment_status as payment_status')
            ->where('orders.restaurant_unique_id', '=', Auth::user()->unique_id)
            ->where('delivery_schedule_day','=',date("Y-m-d"))
            ->orderBy('orders.id', 'desc')
            ->get();


          $orders = DB::table('orders')
            ->join('users', 'orders.customer_id', '=', 'users.id')
            ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
            ->join('payments', 'orders.id', '=', 'payments.order_id')
            ->select('orders.*', 'shippings.delivery_schedule_day as delivery_schedule_day', 'users.first_name as first_name', 'users.last_name as last_name', 'payments.payment_type as payment_type', 'payments.payment_status as payment_status')
            ->where('orders.restaurant_unique_id', '=', Auth::user()->unique_id)
            ->orderBy('orders.id', 'desc')
            ->get();

        return view('restaurant.order.manage-order', compact('orders','todayOrders'));
    }

    public function viewOrderDetail($id)
    {
        $order = Order::find($id);
        $customer = User::find($order->customer_id);
        $shipping = Shipping::find($order->shipping_id);
        $products = OrderDetail::where('order_id', $order->id)->get();

        return view('restaurant.order.view-order', compact('customer', 'shipping', 'products'));
    }

    public function viewOrderInvoice($id)
    {
        $order = Order::find($id);
        $customer = User::find($order->customer_id);
        $shipping = Shipping::find($order->shipping_id);
        $products = OrderDetail::where('order_id', $order->id)->get();

        return view('restaurant.order.view-invoice', compact('order', 'customer', 'shipping', 'products'));
    }

    public function invoicePDF()
    {
        $order = Order::find(13);
        $customer = User::find($order->customer_id);
        $shipping = Shipping::find($order->shipping_id);
        $products = OrderDetail::where('order_id', $order->id)->get();

        $pdf = PDF::loadView('restaurant.order.download-invoice-pdf', ['order' => $order, 'customer' => $customer, 'shipping' => $shipping, 'products' => $products]);

        return $pdf->download('invoice.pdf');
    }

    public function editOrderInfo($id)
    {
        $order = Order::find($id);
        $payment = Payment::where('order_id', $id)->first();
        return view('restaurant.order.edit-order', compact('id', 'order', 'payment'));
    }

    public function updateOrderInfo(Request $request)
    {
        $this->validate($request,[
            'order_status' => 'required',
            'payment_status' => 'required'
        ]);

        $id = $request->id;

        $order = Order::find($id);
        $order->order_status = $request->order_status;
        $order->save();


        $payment = Payment::where('order_id', $id)->first();
        $payment->payment_status = $request->payment_status;
        $payment->save();


        $orderDetails = OrderDetail::where('order_id', $id)->get();
        foreach ($orderDetails as $orderDetail) {
            if ($order->order_status == "Successful" && $payment->payment_status == "Successful") {
                $product = RestaurantMenuItem::find($orderDetail->product_id);
                $product->sales_count += 1;
                $product->save();
            }
        }
        return redirect('/manage-order')->with('message', 'Update Order Info Successful');


    }


    public function deleteOrder($id)
    {
        $order = Order::find($id);

        $orderDetails = OrderDetail::where('order_id', $id)->get();
        foreach ($orderDetails as $orderDetail) {
            $orderDetail->delete();
        }


        $payment = Payment::where('order_id', $id)->first();;
        $payment->delete();

        $shipping = Shipping::find($order->shipping_id);
        $shipping->delete();

         $products = OrderDetail::where('order_id', $order->id)->get();
        foreach ($products as $product) {
            $product->delete();
        }


        $order->delete();
        return redirect()->back()->with('destroy', 'Order Info Delete Successful!!');


    }
}
