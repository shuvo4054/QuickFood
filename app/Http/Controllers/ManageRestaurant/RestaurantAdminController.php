<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\Restaurant;
use App\Http\Controllers\Controller;
use App\RestaurantInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RestaurantAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }

    public function showRestaurantAdminProfile()
    {
        $restaurantAdmin = Restaurant::where('unique_id',Auth::user()->unique_id)->first();
        return view('restaurant.restaurant-admin.view-restaurant-admin-info',compact('restaurantAdmin'));
    }

    public function updateRestaurantProfile(Request $request, $id) {

        $this->validate($request, [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'mobile_number' => 'required|size:11|regex:/(01)[0-9]{9}/',




        ]);


        $restaurantAdmin = Restaurant::find($id);
        $restaurantAdmin->first_name = $request->first_name;
        $restaurantAdmin->last_name = $request->last_name;
        $restaurantAdmin->mobile_number = $request->mobile_number;

        $restaurantAdmin->save();

        return redirect()->back()->with('message','Your Profile Updated Successfully !!');
    }


    public function updateRestaurantProfilePassword(Request $request, $id) {

        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',

        ]);
        $restaurantAdmin = Restaurant::find($id);

        if(password_verify($request->old_password, $restaurantAdmin->password)) {
            $restaurantAdmin->password = bcrypt($request->password);
            $restaurantAdmin->save();
            return redirect()->back()->with('message','Your Password Updated Successfully !!');
        } else{
            return redirect()->back();
        }


    }


     public function updateRestaurantProfileEmail(Request $request, $id) {

         $this->validate($request, [
             'old_email' => 'required|string|email',
             'email' => 'required|string|confirmed|email|max:100|unique:restaurants',

         ]);


         $restaurantAdmin = Restaurant::find($id);
         $restaurantAdmin->email = $request->email;
         $restaurantAdmin->save();

         return redirect()->back()->with('message','Your Email Updated Successfully !!');
     }


     public function coverPicture(){
        return $coverPicture = RestaurantInfo::where('restaurant_id','=',Auth::user()->unique_id)->first();
     }


}
