<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\Ingredient;
use App\RestaurantMenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ItemIngredientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }

    public function manageItemIngredientInfo($id)
    {
        $ingredients =  Ingredient::where('restaurant_menu_items_id','=',$id)->get();
        return view('restaurant.restaurant-food-menu.ingredient.manage-item-ingredient',compact('ingredients','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveItemIngredientInfo(Request $request)
    {
        $this->validate($request, [
            'ingredient_name' => 'required|string',
            'ingredient_price' => 'required|numeric',


        ]);
        $ingredient = new Ingredient();

        $ingredient->restaurant_unique_id = Auth::user()->unique_id;
        $ingredient->restaurant_menu_items_id = $request->restaurant_menu_items_id;
        $ingredient->ingredient_name = $request->ingredient_name;
        $ingredient->ingredient_price = $request->ingredient_price;
        $ingredient->save();

        return redirect()->back()->with('message', 'Item Ingredient Info Inserted Successfully');
    }








    public function deleteItemIngredientInfo($id){
        $ingredient = Ingredient::find($id);
        $ingredient->delete();
        return redirect()->back()->with('destroy','Item Ingredient Info Deleted Successfully');

    }



}
