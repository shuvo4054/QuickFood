<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\RestaurantMenu;
use App\RestaurantMenuItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Image;

class RestaurantMenuItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }

    public function manageRestaurantMenuItemInfo()
    {
        $menus =  RestaurantMenu::where('restaurant_unique_id',Auth::user()->unique_id)->get();
         $menuItems =  RestaurantMenuItem::where('restaurant_unique_id',Auth::user()->unique_id)->get();
        return view('restaurant.restaurant-food-menu.item.manage-menu-item',compact('menuItems','menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveRestaurantMenuItemInfo(Request $request)
    {

        $this->validate($request, [
            'item_name' => 'required|string',
            'item_description' => 'required|string|max:250',
            'item_price' => 'required|numeric',
            'item_image' => 'required|image',
            'publication_status' => 'required|string',


        ]);



        $itemImage = $request->file('item_image');
        $uniqueName = substr(md5(time()),'0','10');
        $uniqueImageName = $uniqueName.'.'.$itemImage->getClientOriginalExtension();
        $directory = 'images/restaurnat-food-item-image/';
        $imageUrl = $directory.$uniqueImageName;
        // $itemImage->move($directory.$uniqueImageName);
        Image::make($itemImage)->save($imageUrl);

        $menuItem = new RestaurantMenuItem();
        $menuItem->restaurant_unique_id = Auth::user()->unique_id;
        $menuItem->restaurant_menu_id = $request->restaurant_menu_id;
        $menuItem->item_name = $request->item_name;
        $menuItem->item_description = $request->item_description;
        $menuItem->item_image = $imageUrl;
        $menuItem->item_price = $request->item_price;
        $menuItem->publication_status = $request->publication_status;
        $menuItem->save();


        return redirect()->back()->with('message', 'Restaurant Food Menu Item Info Inserted Successfully');
    }

    public function unpublishedRestaurantMenuItemInfo($data){
        $menuItemById = RestaurantMenuItem::find($data);
        $menuItemById->publication_status = 0;
        $menuItemById->save();

        return redirect()->back()->with('destroy','Restaurant Food Menu Item unpublished');


    }

    public function publishedRestaurantMenuItemInfo($data){
        $menuItemById = RestaurantMenuItem::find($data);
        $menuItemById->publication_status = 1;
        $menuItemById->save();

        return redirect()->back()->with('message','Restaurant Food Menu Item published');


    }


    public function editRestaurantMenuItemInfo($id)
    {
        $menus =  RestaurantMenu::where('restaurant_unique_id',Auth::user()->unique_id)->get();
        $menuItem = RestaurantMenuItem::find($id);
        return view('restaurant.restaurant-food-menu.item.edit-menu-item',compact('menuItem','menus'));
    }



    public function updateRestaurantMenuItemInfo(Request $request, $id){

        $this->validate($request, [
            'item_name' => 'required|string',
            'item_description' => 'required|string|max:250',
            'item_price' => 'required|numeric',
            'item_image' => 'required|image',
            'publication_status' => 'required|string',


        ]);


        $menuItem = RestaurantMenuItem::find($id);

         $itemImage = $menuItem->item_image;

        if ($request->file('item_image')) {
            @unlink($itemImage);

            $itemImage = $request->file('item_image');
            $uniqueName = substr(md5(time()),'0','10');
            $uniqueImageName = $uniqueName.'.'.$itemImage->getClientOriginalExtension();
            $directory = 'images/restaurnat-food-item-image/';
            $imageUrl = $directory.$uniqueImageName;
            // $itemImage->move($directory.$uniqueImageName);
            Image::make($itemImage)->save($imageUrl);


            $menuItem->item_image = $imageUrl;

        } else {
            $menuItem->item_image = $itemImage;
        }

        $menuItem->item_name = $request->item_name;
        $menuItem->item_description = $request->item_description;
        $menuItem->item_price = $request->item_price;
        $menuItem->publication_status = $request->publication_status;
        $menuItem->save();
        return redirect()->action('ManageRestaurant\RestaurantMenuItemController@manageRestaurantMenuItemInfo')->with('message', 'Restaurant Food Menu Info Updated Successfully');
    }



    public function deleteRestaurantMenuItemInfo($id){
        $menuItem = RestaurantMenuItem::find($id);
        @unlink($menuItem->item_image);
        $menuItem->delete();
        return redirect()->back()->with('destroy','Restaurant Food Menu Item Deleted Successfully');

    }
}
