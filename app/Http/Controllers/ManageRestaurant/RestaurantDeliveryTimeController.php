<?php

namespace App\Http\Controllers\ManageRestaurant;

use App\DeliveryTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RestaurantDeliveryTimeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }


    public function manageDeliveryTime(){
         $deliveryTimes = DeliveryTime::where('restaurant_unique_id','=',Auth::user()->unique_id)->get();
        return view('restaurant.delivery-time.manage-delivery-time',compact('deliveryTimes'));
    }

    public function saveDeliveryTime(Request $request){
        $this->validate($request,[
            'time' => 'required|string'
        ]);

        $time = new DeliveryTime();
        $time->restaurant_unique_id = Auth::user()->unique_id;
        $time->time = $request->time;
        $time->save();

        return redirect()->back()->with('message','Delivery Time Insert Successfully !!');

    }

    public function destroyDeliveryTime($id){
        $time = DeliveryTime::find($id);
        $time->delete();
        return redirect()->back()->with('destroy','Delete Delivery Successfully');
    }
}
