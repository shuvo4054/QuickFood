<?php

namespace App\Http\Controllers;

use App\Order;
use App\RestaurantInfo;
use App\Shipping;
use Illuminate\Support\Facades\Auth;


class RestaurantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:restaurant');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $coverPicture = RestaurantInfo::where('restaurant_id','=',Auth::user()->unique_id)->first();


         $newOrder = Order::where('order_status','=','Pending')->where('restaurant_unique_id','=',Auth::user()->unique_id)->count();
         $successfulOrder = Order::where('order_status','=','Successful')->where('restaurant_unique_id','=',Auth::user()->unique_id)->count();
         $cancelOrder = Order::where('order_status','=','Cancel')->where('restaurant_unique_id','=',Auth::user()->unique_id)->count();

         $todayOrder = Shipping::where('delivery_schedule_day','=',date("Y-m-d"))->where('restaurant_unique_id','=',Auth::user()->unique_id)->count();




        return view('restaurant.home',compact('coverPicture','order','newOrder','todayOrder','successfulOrder','cancelOrder'));
    }
}
