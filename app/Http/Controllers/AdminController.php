<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Restaurant;
use App\RestaurantInfo;
use App\User;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $totalRider = Driver::where('hire','=','1')->count();
        $newRegisterRider = Driver::where('hire','=','0')->count();
         $totalRestaurantCount = Restaurant::count();
         $restaurantRequestCount = RestaurantInfo::where('verify','=',0)->count();
         $userCount = User::count();
         return view('admin.home',compact('totalRestaurantCount','restaurantRequestCount','userCount','totalRider','newRegisterRider'));
    }
}
