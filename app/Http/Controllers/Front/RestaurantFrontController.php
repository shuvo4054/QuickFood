<?php

namespace App\Http\Controllers\Front;

use App\City;
use App\DeliveryTime;
use App\RestaurantInfo;
use App\RestaurantMenu;
use App\RestaurantMenuItem;
use App\Shipping;
use App\SubheaderImage;
use App\UserReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RestaurantFrontController extends Controller
{
   /* public function __construct() {
        $this->middleware('auth:restaurant');
        $this->middleware('guest:restaurant');

    }*/


   public function frontPage(){
       $subHeaderImage = SubheaderImage::first();
       $restaurants = RestaurantInfo::where('popularity','=',1)->where('verify','=','1')->take(6)->get();
       return view('front.home',compact('restaurants','subHeaderImage'));
   }



    // Algolia Search
    public function search(Request $request) {
        $cities = City::all();
        $searchKey = $request->search_key;
        $subHeaderImage = SubheaderImage::first();
        $restaurants = RestaurantInfo::search($searchKey)->paginate(8);
        return view('front.restaurant.algolia-search-restaurant-grid-list',compact('restaurants','cities','subHeaderImage'));
    }


    // Restaurant Grid List
    public function showRestaurantGridList()
    {

        $subHeaderImage = SubheaderImage::first();

        $cities = City::all();
         $restaurants = RestaurantInfo::where('verify','=','1')->paginate(8);

        return view('front.restaurant.restaurant-grid-list',compact('restaurants','cities','subHeaderImage'));
    }



    public static function allCuisines($id)
    {

        $restaurant=DB::table('cuisines')->select('cuisines_name')
            ->leftJoin('restaurant_infos','restaurant_infos.id','=','cuisines.restaurant_infos_id')
            ->where('cuisines.restaurant_infos_id','=',$id)
            ->get();
        return $restaurant;
    }




    //Search By City

    public function searchByCity($cityName)
    {

        if ($cityName == "all") {
            $subHeaderImage = SubheaderImage::first();
            $cities = City::all();
            $restaurants = RestaurantInfo::where('verify','=','1')->paginate(8);
            return view('front.restaurant.restaurant-search-city',compact('restaurants','cities','cityName','subHeaderImage'));
        }

        $cities = City::all();
        $subHeaderImage = SubheaderImage::first();
        $restaurants = RestaurantInfo::where('city','=',$cityName)->where('verify','=','1')->paginate(8);

        return view('front.restaurant.restaurant-search-city',compact('restaurants','cities','cityName','subHeaderImage'));
    }

    // Search by service
    public function searchByService($service)
        {

            if ($service == "delivery") {
                $cities = City::all();
                $subHeaderImage = SubheaderImage::first();
                $restaurants = RestaurantInfo::where('delivery_service','=','yes')->where('verify','=','1')->paginate(8);
                return view('front.restaurant.restaurant-search-service',compact('restaurants','cities','service','subHeaderImage'));
            } else {
                $cities = City::all();
                $subHeaderImage = SubheaderImage::first();
                $restaurants = RestaurantInfo::where('take_way_service','=','yes')->where('verify','=','1')->paginate(8);
                return view('front.restaurant.restaurant-search-service',compact('restaurants','cities','service','subHeaderImage'));
            }


        }


     // Search by Rating
    public function searchByRating($data) {


         $restaurantArrayIds = $this->getRating($data);

        $restaurants = array();

       foreach ($restaurantArrayIds as $value) {
           $restaurants[] = RestaurantInfo::where('restaurant_id','=',$value)->where('verify','=','1')->first();
       }

        $subHeaderImage = SubheaderImage::first();
       $cities = City::all();
         return view('front.restaurant.restaurant-search-rating',compact('restaurants','cities','subHeaderImage','data'));

    }


    private function getRating($data) {
        $a = array();

        $restaurants1 = RestaurantInfo::where('verify','=','1')->get();
        foreach ($restaurants1 as $restaurant) {
            $reviews = UserReview::where('restaurant_unique_id','=',$restaurant->restaurant_id)

                ->select(
                    array(
                        DB::raw('AVG(food_review) as food_review' ),
                        DB::raw('AVG(price_review) as price_review' ),
                        DB::raw('AVG(punctuality_review) as punctuality_review' ),
                        DB::raw('AVG(courtesy_review) as courtesy_review' ),

                    ))

                ->orderBy('user_reviews.id', 'DESC')
                ->groupBy('user_reviews.id')
                ->get();

            $count = UserReview::where('restaurant_unique_id','=',$restaurant->restaurant_id)->count();

            $foodReview = 0;
            $priceReview = 0;
            $punctualityReview = 0;
            $courtesyReview = 0;

            foreach ($reviews as $review){

                $foodReview = $review->food_review + $foodReview;
                $priceReview = $review->price_review + $priceReview;
                $punctualityReview = $review->punctuality_review + $punctualityReview;
                $courtesyReview = $review->courtesy_review + $courtesyReview;
            }

            if($count > 0) {
                $sum = (($foodReview/$count) +($priceReview/$count) +($punctualityReview/$count) +($courtesyReview/$count)) / 4;
                if (intval($sum) == $data) {
                    array_push($a,$restaurant->restaurant_id);
                }
            }

        }

        return $a;
    }


    // End Search By Rating













    // Restaurant Menu Details
    public function showRestaurantMenu($id)
    {
        $restaurant = RestaurantInfo::where('restaurant_id','=', "$id")->first();
        $reviewCount = UserReview::where('restaurant_unique_id','=',$id)->count();

        $menus = RestaurantMenu::where('restaurant_unique_id', "$id")
            ->where('publication_status', '=',1)
            ->get();


        $data = DB::table('restaurant_infos')
            ->join('restaurant_menus', 'restaurant_menus.restaurant_unique_id', '=', 'restaurant_infos.restaurant_id')
            ->join('restaurant_menu_items', 'restaurant_menu_items.restaurant_menu_id', '=', 'restaurant_menus.id')
            ->select('restaurant_infos.restaurant_name','restaurant_menus.*','restaurant_menu_items.*')
           ->where('restaurant_infos.restaurant_id','=',"$id")

            ->get();

        return view('front.restaurant.restaurant-menu-detail',compact('data','menus','restaurant','reviewCount'));
    }


    public static function  countMenuItem($id) {
        $menuItemCount = RestaurantMenuItem::where('restaurant_menu_id','=',$id)
            ->where('publication_status', '=',1)
            ->count();
        return $menuItemCount;
    }




    public static function allorders($id)
    {
        $data=DB::table('ingredients')->select('ingredients.*')
            ->leftJoin('restaurant_menu_items','restaurant_menu_items.id','=','ingredients.restaurant_menu_items_id')
            ->where('ingredients.restaurant_menu_items_id','=',$id)
            ->get();
        return $data;
    }


    public function subHeaderImage(){
       return $subHeaderImage = SubheaderImage::first();
    }


    public function restaurantServiceTime() {
        return $restaurant = RestaurantInfo::where('restaurant_id','=',Session::get('restaurant_unique_id'))->first();
    }

    public function deliveryTime() {
        return $deliveryTimes = DeliveryTime::where('restaurant_unique_id','=',Session::get('restaurant_unique_id'))->get();
    }

    public function deliveryType(){
        return $deliveryType = Shipping::where('id','=',Session::get('shippingId'))->first();
    }




}
