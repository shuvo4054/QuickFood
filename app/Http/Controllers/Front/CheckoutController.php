<?php

namespace App\Http\Controllers\Front;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\Payment;
use App\Shipping;
use Session;
use Mail;
use Cart;
use Illuminate\Support\Facades\Auth;
use function Sodium\compare;


class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('showShippingInfo', 'saveShippingInfo','saveOrderInfo');
    }


    public function viewCheckoutRegisterLoginForm()
    {
        return view('front.checkout.checkout');
    }

    public function saveCustomerInfo(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'mobile_number' => 'required|size:11|regex:/(01)[0-9]{9}/',
            'email' => 'required|string|email|max:255|unique:users',
            'city' => 'required|string|max:100',
            'living_address' => 'required|string|max:100',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $customer = new User();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->email = $request->email;

        $customer->mobile_number = $request->mobile_number;
        $customer->city = $request->city;
        $customer->living_address = $request->living_address;
        $customer->password = bcrypt($request->password);
        $customer->save();

        if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password])) {
            // if successful, then redirect to their intended location
            return redirect('/shipping-info');
        }




        Session::put('customerId', $customer->id);
        Session::put('customerName', $customer->first_name . ' ' . $customer->last_name);

        $data = $customer->toArray();
        /* convert $customer object to array
           to access this array data we have to write
            $data['first_name']
        */

        /*
        Mail::send('front.mail.congratulation-mail', $data, function ($message) use ($data) {
            $message->to($data['email']);// In this position we can't use object. We must use array to pass data
            $message->subject('Congratulation Mail');
        });*/
        /*
         * 'front.mail.congratulation-mail' = this is a blade file where we write our mail
         * $data = this $data contain all data like first_name, last_name, email etc.
         * */

        // Attempt to log the user in



    }



    public function customerLoginCheck(Request $request)
    {

        $customer = User::where('email', $request->email)->first();

        if ($customer) {
            if (password_verify($request->password, $customer->password)) {

                Session::put('customerId', $customer->id);
                Session::put('customerName', $customer->first_name . ' ' . $customer->last_name);


                if (Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password])) {
                    // if successful, then redirect to their intended location
                    return redirect('/shipping-info');
                }

            } else {
                return redirect('/checkout')->with('danger', 'Invalid Password');
            }
        } else {
            return redirect('/checkout')->with('danger', 'Invalid Email');
        }

    }


    public function showShippingInfo()
    {
         $cartProducts = Cart::content();
        $customer = User::find(Auth::user()->id);
        return view('front.checkout.shipping-info', compact('customer','cartProducts'));
    }



    public function saveShippingInfo(Request $request)
    {


        $this->validate($request, [
            'full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|string|email|max:255',
            'mobile_number' => 'required|size:11|regex:/(01)[0-9]{9}/',
            'city' => 'required|string|max:100',
            'living_address' => 'required|string|max:200',
            'delivery_schedule_day' => 'required|date|after_or_equal:date',
            'delivery_schedule_time' => 'required|string|max:100',
            'option_2' => 'required',

        ]);


        $shipping = new Shipping();
        $shipping->restaurant_unique_id = Session::get('restaurant_unique_id') ;
        $shipping->full_name = $request->full_name;
        $shipping->email = $request->email;
        $shipping->mobile_number = $request->mobile_number;
        $shipping->city = $request->city;
        $shipping->living_address = $request->living_address;
        $shipping->delivery_schedule_day = $request->delivery_schedule_day;
        $shipping->delivery_schedule_time = $request->delivery_schedule_time;
        $shipping->service_type = $request->option_2;
        $shipping->save();

        Session::put('shippingId', $shipping->id);


         $cartProducts = Cart::content();

        return view('front.checkout.payment-form',compact('cartProducts'));
    }

    public function showPaymentForm()
    {
        return view('front.checkout.payment-form');
    }

    public function saveOrderInfo(Request $request)
    {



        $this->validate($request, [
            'payment_type' => 'required',

        ]);



        if (!Session::get('grandTotal')) {
            return redirect('/');
        }

        $paymentType = $request->payment_type;
        if ($paymentType == 'Cash On Delivery') {
            $order = new Order();
            $order->restaurant_unique_id = Session::get('restaurant_unique_id');
            $order->customer_id = Auth::user()->id;
            $order->shipping_id = Session::get('shippingId');
            $order->order_total = Session::get('grandTotal');
            $order->save();
            $recentOrderId = $order->id;


            $payment = new Payment();
            $payment->restaurant_unique_id = Session::get('restaurant_unique_id');
            $payment->order_id = $order->id;
            $payment->payment_type = $paymentType;
            $payment->save();

            $cartProducts = Cart::content();
            foreach ($cartProducts as $cartProduct) {
                $orderDetail = new OrderDetail();
                $orderDetail->restaurant_unique_id = Session::get('restaurant_unique_id');
                $orderDetail->order_id = $order->id;
                $orderDetail->product_id = $cartProduct->id;
                $orderDetail->product_name = $cartProduct->name;
                $orderDetail->product_price = $cartProduct->price;
                $orderDetail->product_quantity = $cartProduct->qty;
                $orderDetail->ingredient = $cartProduct->options->ingredient;
                $orderDetail->image = $cartProduct->options->image;

                $orderDetail->save();

            }


            // Here Destroy All Session
            Session::forget('customerId');
            Session::forget('customerIdName');
            Session::forget('delivery_fee');
            Session::forget('minimum_order');
            Session::forget('restaurant_unique_id');
            Session::forget('grandTotal');
            Cart::destroy();



            $order = Order::find($recentOrderId);
            $shipping = Shipping::find($order->shipping_id);
            $products = OrderDetail::where('order_id', $order->id)->get();

//            return redirect('/confirm-order',compact('order','shipping','products'));
            return view('front.checkout.confirm-order',compact('order','shipping','products'));


        }


    }
}

