<?php

namespace App\Http\Controllers\Front;

use App\Driver;
use App\FaqContent;
use App\FaqTitel;
use App\RestaurantInfo;
use App\SubheaderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherViewController extends Controller
{
    public function viewAboutUs() {
        return view('front.about-us.about-us');
    }

    public function viewFAQ() {
        $faqTitles = FaqTitel::where('publication_status','=',1)->get();
        $faqContents = FaqContent::where('publication_status','=',1)->get();
        return view('front.faq.faq',compact('faqContents','faqTitles'));
    }



    // Faq
    public function showRestaurantMenu()
    {
        $faqTitle = FaqTitel::all();
        $faqContent = FaqContent::all();
     return view('front.faq.faq',compact('faqContent','faqTitle'));
    }

    public static function allorders($id)
    {
        $data=DB::table('orders')->select('ingredient')
            ->leftJoin('items','items.id','=','orders.item_id')
            ->where('orders.item_id','=',$id)
            ->get();
        return $data;
    }


    // Submit Restaurant
    public function viewSubmitDriverForm(){
         $subHeaderImage =  SubheaderImage::first();
        return view('front.driver.driver',compact('subHeaderImage'));
    }


    public function saveSubmitDriverForm(Request $request){
        $this->validate($request, [
            'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'mobile_number' => 'required|size:11|regex:/(01)[0-9]{9}/',
            'email' => 'required|string|email|max:255',
            'city' => 'required|string|max:100',
            'living_address' => 'required|string|max:100',
            'motor' => 'required',
            'license' => 'required',

        ]);


        $driver = new Driver();
        $driver->first_name = $request->first_name;
        $driver->last_name = $request->last_name;
        $driver->mobile_number = $request->mobile_number;
        $driver->email = $request->email;
        $driver->city = $request->city;
        $driver->living_address = $request->living_address;
        $driver->motor = $request->motor;
        $driver->license = $request->license;

        $driver->save();
        return redirect()->back()->with('message','Your Form Submit Successfully');


    }










}
