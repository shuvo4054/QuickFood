<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class RestaurantInfo extends Model
{
    use Searchable;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
   /* public function toSearchableArray()
    {
        $record = $this->toArray();

        $record['_restaurant_name'] = $record['restaurant_name'];

        $record['_city'] = $record['city'];

        $record['_restaurant_address'] = $record['restaurant_address'];

        $record['_delivery_zone'] = $record['delivery_zone'];

        unset($record['restaurant_name'], $record['city'], $record['restaurant_address'], $record['delivery_zone']);

        return $record;
    }*/


    public function searchableAs()
    {
        return 'restaurant_info';
    }
}
